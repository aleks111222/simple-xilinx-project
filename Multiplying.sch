<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="Vt0" />
        <signal name="Vtb0" />
        <signal name="UceM" />
        <signal name="UclrM" />
        <signal name="XLXN_237" />
        <signal name="UceTinit" />
        <signal name="UclrTinit" />
        <signal name="Tin(7:0)" />
        <signal name="XLXN_266" />
        <signal name="XLXN_275" />
        <signal name="XLXN_284" />
        <signal name="UceT" />
        <signal name="UleT" />
        <signal name="UclrT" />
        <signal name="XLXN_293" />
        <signal name="XLXN_20" />
        <signal name="XLXN_258" />
        <signal name="XLXN_298(7:0)" />
        <signal name="Q(7:0)" />
        <signal name="Q(6)" />
        <signal name="Q(0)" />
        <signal name="Q(1)" />
        <signal name="Q(2)" />
        <signal name="Q(3)" />
        <signal name="Q(4)" />
        <signal name="Q(5)" />
        <signal name="Q(7)" />
        <signal name="XLXN_309" />
        <signal name="XLXN_216" />
        <signal name="XLXN_215" />
        <signal name="XLXN_223" />
        <signal name="UceZ" />
        <signal name="UclrZ" />
        <signal name="UleZ" />
        <signal name="XLXN_335" />
        <signal name="XLXN_342(7:0)" />
        <signal name="D(15:0)" />
        <signal name="XLXN_345" />
        <signal name="D(15)" />
        <signal name="XLXN_356(7:0)" />
        <signal name="Z(7:0)" />
        <signal name="XLXN_359(7:0)" />
        <signal name="XLXN_340" />
        <signal name="D(14)" />
        <signal name="D(13)" />
        <signal name="D(12)" />
        <signal name="D(11)" />
        <signal name="D(10)" />
        <signal name="D(9)" />
        <signal name="D(8)" />
        <signal name="Z(7)" />
        <signal name="D(7)" />
        <signal name="Z(6)" />
        <signal name="D(6)" />
        <signal name="D(5)" />
        <signal name="Z(5)" />
        <signal name="Z(4)" />
        <signal name="D(4)" />
        <signal name="D(3)" />
        <signal name="Z(3)" />
        <signal name="D(2)" />
        <signal name="Z(2)" />
        <signal name="D(1)" />
        <signal name="Z(1)" />
        <signal name="D(0)" />
        <signal name="Z(0)" />
        <signal name="XLXN_385" />
        <signal name="XLXN_386(15:0)" />
        <signal name="Y(15:0)" />
        <signal name="XLXN_389(15:0)" />
        <signal name="XLXN_390(15:0)" />
        <signal name="XLXN_391(15:0)" />
        <signal name="XLXN_399" />
        <signal name="XLXN_400" />
        <signal name="XLXN_401" />
        <signal name="XLXN_403" />
        <signal name="XLXN_404" />
        <signal name="XLXN_405" />
        <signal name="UceSUMM" />
        <signal name="UclrSUMM" />
        <signal name="XLXN_410(15:0)" />
        <signal name="OUTT(15:0)" />
        <signal name="XLXN_412" />
        <signal name="XLXN_413(15:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="Vt0" />
        <port polarity="Output" name="Vtb0" />
        <port polarity="Input" name="UceM" />
        <port polarity="Input" name="UclrM" />
        <port polarity="Input" name="UceTinit" />
        <port polarity="Input" name="UclrTinit" />
        <port polarity="Input" name="Tin(7:0)" />
        <port polarity="Input" name="UceT" />
        <port polarity="Input" name="UleT" />
        <port polarity="Input" name="UclrT" />
        <port polarity="Input" name="UceZ" />
        <port polarity="Input" name="UclrZ" />
        <port polarity="Input" name="UleZ" />
        <port polarity="Input" name="Z(7:0)" />
        <port polarity="Output" name="Y(15:0)" />
        <port polarity="Input" name="UceSUMM" />
        <port polarity="Input" name="UclrSUMM" />
        <port polarity="Output" name="OUTT(15:0)" />
        <blockdef name="sr16cled">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-512" y2="-512" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-384" y2="-384" x1="384" />
            <line x2="64" y1="-576" y2="-576" x1="0" />
            <rect width="64" x="0" y="-524" height="24" />
            <rect width="64" x="320" y="-396" height="24" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-640" height="576" />
        </blockdef>
        <blockdef name="sr8cled">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-512" y2="-512" x1="0" />
            <rect width="64" x="0" y="-524" height="24" />
            <line x2="64" y1="-576" y2="-576" x1="0" />
            <rect width="64" x="320" y="-396" height="24" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="320" y1="-384" y2="-384" x1="384" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <rect width="256" x="64" y="-640" height="576" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="nor8">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <arc ex="48" ey="-336" sx="48" sy="-240" r="56" cx="16" cy="-288" />
            <line x2="64" y1="-336" y2="-336" x1="128" />
            <line x2="64" y1="-240" y2="-240" x1="128" />
            <arc ex="208" ey="-288" sx="128" sy="-240" r="88" cx="132" cy="-328" />
            <arc ex="128" ey="-336" sx="208" sy="-288" r="88" cx="132" cy="-248" />
            <line x2="228" y1="-288" y2="-288" x1="256" />
            <circle r="10" cx="218" cy="-286" />
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="48" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-384" y2="-384" x1="0" />
            <line x2="48" y1="-448" y2="-448" x1="0" />
            <line x2="48" y1="-512" y2="-512" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="48" y1="-336" y2="-512" x1="48" />
            <line x2="48" y1="-64" y2="-240" x1="48" />
            <line x2="48" y1="-336" y2="-336" x1="72" />
            <line x2="52" y1="-240" y2="-240" x1="72" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <block symbolname="nor8" name="XLXI_65">
            <blockpin signalname="Q(7)" name="I0" />
            <blockpin signalname="Q(6)" name="I1" />
            <blockpin signalname="Q(5)" name="I2" />
            <blockpin signalname="Q(4)" name="I3" />
            <blockpin signalname="Q(3)" name="I4" />
            <blockpin signalname="Q(2)" name="I5" />
            <blockpin signalname="Q(1)" name="I6" />
            <blockpin signalname="Q(0)" name="I7" />
            <blockpin signalname="Vt0" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_66">
            <blockpin signalname="XLXN_309" name="I" />
            <blockpin signalname="Vtb0" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_90">
            <blockpin signalname="Q(0)" name="I" />
            <blockpin signalname="XLXN_309" name="O" />
        </block>
        <block symbolname="fd16ce" name="XLXI_94">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="UceM" name="CE" />
            <blockpin signalname="UclrM" name="CLR" />
            <blockpin signalname="OUTT(15:0)" name="D(15:0)" />
            <blockpin signalname="Y(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_95">
            <blockpin signalname="XLXN_237" name="G" />
        </block>
        <block symbolname="fd8ce" name="XLXI_100">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="UceTinit" name="CE" />
            <blockpin signalname="UclrTinit" name="CLR" />
            <blockpin signalname="Tin(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_298(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_42">
            <blockpin signalname="XLXN_20" name="G" />
        </block>
        <block symbolname="sr8cled" name="XLXI_109">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="UceT" name="CE" />
            <blockpin signalname="UclrT" name="CLR" />
            <blockpin signalname="XLXN_298(7:0)" name="D(7:0)" />
            <blockpin signalname="UleT" name="L" />
            <blockpin signalname="XLXN_20" name="LEFT" />
            <blockpin signalname="XLXN_258" name="SLI" />
            <blockpin signalname="XLXN_20" name="SRI" />
            <blockpin signalname="Q(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="sr16cled" name="XLXI_33">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="UceZ" name="CE" />
            <blockpin signalname="UclrZ" name="CLR" />
            <blockpin signalname="D(15:0)" name="D(15:0)" />
            <blockpin signalname="UleZ" name="L" />
            <blockpin signalname="XLXN_223" name="LEFT" />
            <blockpin signalname="XLXN_216" name="SLI" />
            <blockpin signalname="XLXN_215" name="SRI" />
            <blockpin signalname="XLXN_391(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_91">
            <blockpin signalname="XLXN_216" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_92">
            <blockpin signalname="XLXN_215" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_93">
            <blockpin signalname="XLXN_223" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_122">
            <blockpin signalname="D(15)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_123">
            <blockpin signalname="D(14)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_124">
            <blockpin signalname="D(13)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_125">
            <blockpin signalname="D(12)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_126">
            <blockpin signalname="D(11)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_127">
            <blockpin signalname="D(10)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_128">
            <blockpin signalname="D(9)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_129">
            <blockpin signalname="D(8)" name="G" />
        </block>
        <block symbolname="buf" name="XLXI_130">
            <blockpin signalname="Z(0)" name="I" />
            <blockpin signalname="D(0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_131">
            <blockpin signalname="Z(1)" name="I" />
            <blockpin signalname="D(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_132">
            <blockpin signalname="Z(2)" name="I" />
            <blockpin signalname="D(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_133">
            <blockpin signalname="Z(3)" name="I" />
            <blockpin signalname="D(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_134">
            <blockpin signalname="Z(4)" name="I" />
            <blockpin signalname="D(4)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_135">
            <blockpin signalname="Z(5)" name="I" />
            <blockpin signalname="D(5)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_136">
            <blockpin signalname="Z(6)" name="I" />
            <blockpin signalname="D(6)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_137">
            <blockpin signalname="Z(7)" name="I" />
            <blockpin signalname="D(7)" name="O" />
        </block>
        <block symbolname="add16" name="XLXI_139">
            <blockpin signalname="OUTT(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_391(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_237" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_410(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_43">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="UceSUMM" name="CE" />
            <blockpin signalname="UclrSUMM" name="CLR" />
            <blockpin signalname="XLXN_410(15:0)" name="D(15:0)" />
            <blockpin signalname="OUTT(15:0)" name="Q(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="2688" y="608" name="XLXI_65" orien="R270" />
        <branch name="Vt0">
            <wire x2="2400" y1="320" y2="352" x1="2400" />
        </branch>
        <branch name="Vtb0">
            <wire x2="1824" y1="368" y2="368" x1="1760" />
        </branch>
        <iomarker fontsize="28" x="2400" y="320" name="Vt0" orien="R270" />
        <iomarker fontsize="28" x="1760" y="368" name="Vtb0" orien="R180" />
        <instance x="2128" y="624" name="XLXI_90" orien="R270" />
        <instance x="2048" y="336" name="XLXI_66" orien="R180" />
        <instance x="2608" y="1536" name="XLXI_94" orien="R0" />
        <branch name="UceM">
            <wire x2="2608" y1="1344" y2="1344" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2576" y="1344" name="UceM" orien="R180" />
        <branch name="UclrM">
            <wire x2="2608" y1="1504" y2="1504" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2576" y="1504" name="UclrM" orien="R180" />
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2480" y="1408" type="branch" />
            <wire x2="2608" y1="1408" y2="1408" x1="2480" />
        </branch>
        <branch name="XLXN_237">
            <wire x2="2080" y1="2192" y2="2192" x1="2016" />
            <wire x2="2016" y1="2192" y2="2256" x1="2016" />
            <wire x2="2080" y1="2176" y2="2192" x1="2080" />
        </branch>
        <instance x="2144" y="2048" name="XLXI_95" orien="R180" />
        <branch name="UceTinit">
            <wire x2="240" y1="1120" y2="1120" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="1120" name="UceTinit" orien="R180" />
        <branch name="UclrTinit">
            <wire x2="240" y1="1280" y2="1280" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="1280" name="UclrTinit" orien="R180" />
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="192" y="1184" type="branch" />
            <wire x2="240" y1="1184" y2="1184" x1="192" />
        </branch>
        <instance x="240" y="1312" name="XLXI_100" orien="R0" />
        <branch name="Tin(7:0)">
            <wire x2="240" y1="1056" y2="1056" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="1056" name="Tin(7:0)" orien="R180" />
        <branch name="UceT">
            <wire x2="1248" y1="1152" y2="1152" x1="1216" />
        </branch>
        <branch name="CLK">
            <wire x2="1248" y1="1216" y2="1216" x1="1152" />
        </branch>
        <branch name="UleT">
            <wire x2="1248" y1="1024" y2="1024" x1="1168" />
        </branch>
        <branch name="UclrT">
            <wire x2="1248" y1="1312" y2="1312" x1="1184" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="864" y1="1008" y2="1072" x1="864" />
            <wire x2="912" y1="1008" y2="1008" x1="864" />
            <wire x2="912" y1="1008" y2="1088" x1="912" />
            <wire x2="1248" y1="1088" y2="1088" x1="912" />
            <wire x2="1248" y1="896" y2="896" x1="912" />
            <wire x2="912" y1="896" y2="1008" x1="912" />
        </branch>
        <branch name="XLXN_258">
            <wire x2="1232" y1="672" y2="768" x1="1232" />
            <wire x2="1248" y1="768" y2="768" x1="1232" />
        </branch>
        <instance x="800" y="1200" name="XLXI_42" orien="R0" />
        <iomarker fontsize="28" x="1216" y="1152" name="UceT" orien="R180" />
        <iomarker fontsize="28" x="1152" y="1216" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="1184" y="1312" name="UclrT" orien="R180" />
        <iomarker fontsize="28" x="1168" y="1024" name="UleT" orien="R180" />
        <instance x="1248" y="1344" name="XLXI_109" orien="R0" />
        <branch name="XLXN_298(7:0)">
            <wire x2="848" y1="1056" y2="1056" x1="624" />
            <wire x2="848" y1="832" y2="1056" x1="848" />
            <wire x2="1248" y1="832" y2="832" x1="848" />
        </branch>
        <branch name="Q(7:0)">
            <wire x2="2096" y1="960" y2="960" x1="1632" />
            <wire x2="2176" y1="960" y2="960" x1="2096" />
            <wire x2="2272" y1="960" y2="960" x1="2176" />
            <wire x2="2384" y1="960" y2="960" x1="2272" />
            <wire x2="2480" y1="960" y2="960" x1="2384" />
            <wire x2="2576" y1="960" y2="960" x1="2480" />
            <wire x2="2688" y1="960" y2="960" x1="2576" />
            <wire x2="2800" y1="960" y2="960" x1="2688" />
            <wire x2="3024" y1="960" y2="960" x1="2800" />
            <wire x2="3024" y1="960" y2="976" x1="3024" />
        </branch>
        <bustap x2="2096" y1="960" y2="864" x1="2096" />
        <bustap x2="2176" y1="960" y2="864" x1="2176" />
        <bustap x2="2272" y1="960" y2="864" x1="2272" />
        <bustap x2="2384" y1="960" y2="864" x1="2384" />
        <bustap x2="2480" y1="960" y2="864" x1="2480" />
        <bustap x2="2576" y1="960" y2="864" x1="2576" />
        <bustap x2="2688" y1="960" y2="864" x1="2688" />
        <branch name="Q(6)">
            <wire x2="2560" y1="608" y2="672" x1="2560" />
            <wire x2="2688" y1="672" y2="672" x1="2560" />
            <wire x2="2688" y1="672" y2="864" x1="2688" />
        </branch>
        <bustap x2="2800" y1="960" y2="864" x1="2800" />
        <branch name="Q(0)">
            <wire x2="2096" y1="624" y2="736" x1="2096" />
            <wire x2="2096" y1="736" y2="864" x1="2096" />
            <wire x2="2176" y1="736" y2="736" x1="2096" />
            <wire x2="2176" y1="608" y2="736" x1="2176" />
        </branch>
        <branch name="Q(1)">
            <wire x2="2192" y1="864" y2="864" x1="2176" />
            <wire x2="2240" y1="752" y2="752" x1="2192" />
            <wire x2="2192" y1="752" y2="864" x1="2192" />
            <wire x2="2240" y1="608" y2="752" x1="2240" />
        </branch>
        <branch name="Q(2)">
            <wire x2="2304" y1="864" y2="864" x1="2272" />
            <wire x2="2304" y1="608" y2="864" x1="2304" />
        </branch>
        <branch name="Q(3)">
            <wire x2="2368" y1="608" y2="864" x1="2368" />
            <wire x2="2384" y1="864" y2="864" x1="2368" />
        </branch>
        <branch name="Q(4)">
            <wire x2="2432" y1="608" y2="736" x1="2432" />
            <wire x2="2480" y1="736" y2="736" x1="2432" />
            <wire x2="2480" y1="736" y2="864" x1="2480" />
        </branch>
        <branch name="Q(5)">
            <wire x2="2496" y1="608" y2="736" x1="2496" />
            <wire x2="2576" y1="736" y2="736" x1="2496" />
            <wire x2="2576" y1="736" y2="864" x1="2576" />
        </branch>
        <branch name="Q(7)">
            <wire x2="2800" y1="608" y2="608" x1="2624" />
            <wire x2="2800" y1="608" y2="864" x1="2800" />
        </branch>
        <branch name="XLXN_309">
            <wire x2="2096" y1="368" y2="368" x1="2048" />
            <wire x2="2096" y1="368" y2="400" x1="2096" />
        </branch>
        <instance x="1040" y="2304" name="XLXI_91" orien="R0" />
        <branch name="XLXN_216">
            <wire x2="1104" y1="2160" y2="2176" x1="1104" />
            <wire x2="1200" y1="2160" y2="2160" x1="1104" />
            <wire x2="1200" y1="2160" y2="2304" x1="1200" />
            <wire x2="1312" y1="2304" y2="2304" x1="1200" />
        </branch>
        <branch name="XLXN_215">
            <wire x2="1312" y1="2432" y2="2432" x1="1216" />
        </branch>
        <instance x="1088" y="2368" name="XLXI_92" orien="R90" />
        <branch name="XLXN_223">
            <wire x2="1312" y1="2624" y2="2624" x1="1248" />
        </branch>
        <branch name="UceZ">
            <wire x2="1312" y1="2688" y2="2688" x1="1248" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1184" y="2752" type="branch" />
            <wire x2="1312" y1="2752" y2="2752" x1="1184" />
        </branch>
        <branch name="UclrZ">
            <wire x2="1312" y1="2848" y2="2848" x1="1248" />
        </branch>
        <instance x="1248" y="2688" name="XLXI_93" orien="R270" />
        <branch name="UleZ">
            <wire x2="1312" y1="2560" y2="2560" x1="1248" />
        </branch>
        <iomarker fontsize="28" x="1248" y="2688" name="UceZ" orien="R180" />
        <iomarker fontsize="28" x="1248" y="2848" name="UclrZ" orien="R180" />
        <iomarker fontsize="28" x="1248" y="2560" name="UleZ" orien="R180" />
        <instance x="1312" y="2880" name="XLXI_33" orien="R0" />
        <branch name="D(15:0)">
            <wire x2="1024" y1="1696" y2="1792" x1="1024" />
            <wire x2="1024" y1="1792" y2="1936" x1="1024" />
            <wire x2="1024" y1="1936" y2="2032" x1="1024" />
            <wire x2="1024" y1="2032" y2="2112" x1="1024" />
            <wire x2="1024" y1="2112" y2="2224" x1="1024" />
            <wire x2="1024" y1="2224" y2="2336" x1="1024" />
            <wire x2="1024" y1="2336" y2="2368" x1="1024" />
            <wire x2="1312" y1="2368" y2="2368" x1="1024" />
            <wire x2="1024" y1="2368" y2="2448" x1="1024" />
            <wire x2="1024" y1="2448" y2="2576" x1="1024" />
            <wire x2="1024" y1="2576" y2="2752" x1="1024" />
            <wire x2="1024" y1="2752" y2="2848" x1="1024" />
            <wire x2="1024" y1="2848" y2="2928" x1="1024" />
            <wire x2="1024" y1="2928" y2="3040" x1="1024" />
            <wire x2="1024" y1="3040" y2="3120" x1="1024" />
            <wire x2="1024" y1="3120" y2="3200" x1="1024" />
            <wire x2="1024" y1="3200" y2="3280" x1="1024" />
            <wire x2="1024" y1="3280" y2="3360" x1="1024" />
            <wire x2="1024" y1="3360" y2="3392" x1="1024" />
            <wire x2="1040" y1="3392" y2="3392" x1="1024" />
        </branch>
        <bustap x2="928" y1="3360" y2="3360" x1="1024" />
        <bustap x2="928" y1="3280" y2="3280" x1="1024" />
        <bustap x2="928" y1="3200" y2="3200" x1="1024" />
        <bustap x2="928" y1="3120" y2="3120" x1="1024" />
        <bustap x2="928" y1="3040" y2="3040" x1="1024" />
        <bustap x2="928" y1="2928" y2="2928" x1="1024" />
        <bustap x2="928" y1="2848" y2="2848" x1="1024" />
        <bustap x2="928" y1="2752" y2="2752" x1="1024" />
        <branch name="D(15)">
            <wire x2="928" y1="3360" y2="3360" x1="912" />
            <wire x2="912" y1="3360" y2="3424" x1="912" />
            <wire x2="976" y1="3424" y2="3424" x1="912" />
            <wire x2="976" y1="3424" y2="3488" x1="976" />
        </branch>
        <instance x="912" y="3616" name="XLXI_122" orien="R0" />
        <instance x="784" y="3632" name="XLXI_123" orien="R0" />
        <instance x="688" y="3632" name="XLXI_124" orien="R0" />
        <instance x="576" y="3648" name="XLXI_125" orien="R0" />
        <instance x="464" y="3648" name="XLXI_126" orien="R0" />
        <instance x="336" y="3664" name="XLXI_127" orien="R0" />
        <instance x="192" y="3680" name="XLXI_128" orien="R0" />
        <instance x="48" y="3680" name="XLXI_129" orien="R0" />
        <branch name="Z(7:0)">
            <wire x2="208" y1="2496" y2="2496" x1="144" />
            <wire x2="208" y1="1632" y2="1760" x1="208" />
            <wire x2="208" y1="1760" y2="1872" x1="208" />
            <wire x2="208" y1="1872" y2="1952" x1="208" />
            <wire x2="208" y1="1952" y2="2048" x1="208" />
            <wire x2="208" y1="2048" y2="2144" x1="208" />
            <wire x2="208" y1="2144" y2="2240" x1="208" />
            <wire x2="208" y1="2240" y2="2320" x1="208" />
            <wire x2="208" y1="2320" y2="2400" x1="208" />
            <wire x2="208" y1="2400" y2="2496" x1="208" />
        </branch>
        <bustap x2="304" y1="2400" y2="2400" x1="208" />
        <bustap x2="304" y1="2320" y2="2320" x1="208" />
        <bustap x2="304" y1="2240" y2="2240" x1="208" />
        <bustap x2="304" y1="2144" y2="2144" x1="208" />
        <bustap x2="304" y1="2048" y2="2048" x1="208" />
        <bustap x2="304" y1="1952" y2="1952" x1="208" />
        <bustap x2="304" y1="1872" y2="1872" x1="208" />
        <bustap x2="304" y1="1760" y2="1760" x1="208" />
        <iomarker fontsize="28" x="144" y="2496" name="Z(7:0)" orien="R180" />
        <branch name="D(14)">
            <wire x2="928" y1="3280" y2="3280" x1="848" />
            <wire x2="848" y1="3280" y2="3504" x1="848" />
        </branch>
        <branch name="D(13)">
            <wire x2="928" y1="3200" y2="3200" x1="752" />
            <wire x2="752" y1="3200" y2="3504" x1="752" />
        </branch>
        <branch name="D(12)">
            <wire x2="928" y1="3120" y2="3120" x1="640" />
            <wire x2="640" y1="3120" y2="3520" x1="640" />
        </branch>
        <branch name="D(11)">
            <wire x2="928" y1="3040" y2="3040" x1="528" />
            <wire x2="528" y1="3040" y2="3520" x1="528" />
        </branch>
        <branch name="D(10)">
            <wire x2="928" y1="2928" y2="2928" x1="400" />
            <wire x2="400" y1="2928" y2="3536" x1="400" />
        </branch>
        <branch name="D(9)">
            <wire x2="928" y1="2848" y2="2848" x1="256" />
            <wire x2="256" y1="2848" y2="3552" x1="256" />
        </branch>
        <branch name="D(8)">
            <wire x2="928" y1="2752" y2="2752" x1="112" />
            <wire x2="112" y1="2752" y2="3552" x1="112" />
        </branch>
        <bustap x2="928" y1="2576" y2="2576" x1="1024" />
        <bustap x2="928" y1="2448" y2="2448" x1="1024" />
        <bustap x2="928" y1="2336" y2="2336" x1="1024" />
        <bustap x2="928" y1="2224" y2="2224" x1="1024" />
        <bustap x2="928" y1="2112" y2="2112" x1="1024" />
        <bustap x2="928" y1="2032" y2="2032" x1="1024" />
        <bustap x2="928" y1="1936" y2="1936" x1="1024" />
        <bustap x2="928" y1="1792" y2="1792" x1="1024" />
        <instance x="496" y="1792" name="XLXI_130" orien="R0" />
        <instance x="496" y="1904" name="XLXI_131" orien="R0" />
        <instance x="480" y="1984" name="XLXI_132" orien="R0" />
        <instance x="480" y="2096" name="XLXI_133" orien="R0" />
        <instance x="464" y="2240" name="XLXI_134" orien="R0" />
        <instance x="496" y="2368" name="XLXI_135" orien="R0" />
        <instance x="496" y="2448" name="XLXI_136" orien="R0" />
        <instance x="512" y="2608" name="XLXI_137" orien="R0" />
        <branch name="Z(7)">
            <wire x2="400" y1="2400" y2="2400" x1="304" />
            <wire x2="400" y1="2400" y2="2576" x1="400" />
            <wire x2="512" y1="2576" y2="2576" x1="400" />
        </branch>
        <branch name="D(7)">
            <wire x2="928" y1="2576" y2="2576" x1="736" />
        </branch>
        <branch name="Z(6)">
            <wire x2="448" y1="2320" y2="2320" x1="304" />
            <wire x2="448" y1="2320" y2="2416" x1="448" />
            <wire x2="496" y1="2416" y2="2416" x1="448" />
        </branch>
        <branch name="D(6)">
            <wire x2="816" y1="2416" y2="2416" x1="720" />
            <wire x2="816" y1="2416" y2="2448" x1="816" />
            <wire x2="928" y1="2448" y2="2448" x1="816" />
        </branch>
        <branch name="D(5)">
            <wire x2="928" y1="2336" y2="2336" x1="720" />
        </branch>
        <branch name="Z(5)">
            <wire x2="320" y1="2240" y2="2240" x1="304" />
            <wire x2="320" y1="2240" y2="2272" x1="320" />
            <wire x2="496" y1="2272" y2="2272" x1="320" />
            <wire x2="496" y1="2272" y2="2336" x1="496" />
        </branch>
        <branch name="Z(4)">
            <wire x2="384" y1="2144" y2="2144" x1="304" />
            <wire x2="384" y1="2144" y2="2208" x1="384" />
            <wire x2="464" y1="2208" y2="2208" x1="384" />
        </branch>
        <branch name="D(4)">
            <wire x2="800" y1="2208" y2="2208" x1="688" />
            <wire x2="800" y1="2208" y2="2224" x1="800" />
            <wire x2="928" y1="2224" y2="2224" x1="800" />
        </branch>
        <branch name="D(3)">
            <wire x2="816" y1="2064" y2="2064" x1="704" />
            <wire x2="816" y1="2064" y2="2112" x1="816" />
            <wire x2="928" y1="2112" y2="2112" x1="816" />
        </branch>
        <branch name="Z(3)">
            <wire x2="384" y1="2048" y2="2048" x1="304" />
            <wire x2="384" y1="2048" y2="2064" x1="384" />
            <wire x2="480" y1="2064" y2="2064" x1="384" />
        </branch>
        <branch name="D(2)">
            <wire x2="816" y1="1952" y2="1952" x1="704" />
            <wire x2="816" y1="1952" y2="2032" x1="816" />
            <wire x2="928" y1="2032" y2="2032" x1="816" />
        </branch>
        <branch name="Z(2)">
            <wire x2="480" y1="1952" y2="1952" x1="304" />
        </branch>
        <branch name="D(1)">
            <wire x2="816" y1="1872" y2="1872" x1="720" />
            <wire x2="816" y1="1872" y2="1936" x1="816" />
            <wire x2="928" y1="1936" y2="1936" x1="816" />
        </branch>
        <branch name="Z(1)">
            <wire x2="496" y1="1872" y2="1872" x1="304" />
        </branch>
        <branch name="D(0)">
            <wire x2="816" y1="1760" y2="1760" x1="720" />
            <wire x2="816" y1="1760" y2="1792" x1="816" />
            <wire x2="928" y1="1792" y2="1792" x1="816" />
        </branch>
        <branch name="Z(0)">
            <wire x2="496" y1="1760" y2="1760" x1="304" />
        </branch>
        <branch name="Y(15:0)">
            <wire x2="3248" y1="1280" y2="1280" x1="2992" />
        </branch>
        <iomarker fontsize="28" x="3248" y="1280" name="Y(15:0)" orien="R0" />
        <instance x="2016" y="2704" name="XLXI_139" orien="R0" />
        <branch name="XLXN_391(15:0)">
            <wire x2="1856" y1="2496" y2="2496" x1="1696" />
            <wire x2="1856" y1="2496" y2="2512" x1="1856" />
            <wire x2="2016" y1="2512" y2="2512" x1="1856" />
        </branch>
        <branch name="UceSUMM">
            <wire x2="2928" y1="2208" y2="2208" x1="2912" />
            <wire x2="3088" y1="2096" y2="2096" x1="2928" />
            <wire x2="2928" y1="2096" y2="2208" x1="2928" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="2272" type="branch" />
            <wire x2="3008" y1="2272" y2="2272" x1="2912" />
            <wire x2="3008" y1="2160" y2="2272" x1="3008" />
            <wire x2="3088" y1="2160" y2="2160" x1="3008" />
        </branch>
        <branch name="UclrSUMM">
            <wire x2="3088" y1="2368" y2="2368" x1="2912" />
            <wire x2="3088" y1="2256" y2="2368" x1="3088" />
        </branch>
        <instance x="3088" y="2288" name="XLXI_43" orien="R0" />
        <iomarker fontsize="28" x="2912" y="2208" name="UceSUMM" orien="R180" />
        <iomarker fontsize="28" x="2912" y="2368" name="UclrSUMM" orien="R180" />
        <branch name="XLXN_410(15:0)">
            <wire x2="2592" y1="2448" y2="2448" x1="2464" />
            <wire x2="2592" y1="2032" y2="2448" x1="2592" />
            <wire x2="3088" y1="2032" y2="2032" x1="2592" />
        </branch>
        <branch name="OUTT(15:0)">
            <wire x2="1904" y1="1792" y2="2384" x1="1904" />
            <wire x2="1920" y1="2384" y2="2384" x1="1904" />
            <wire x2="2016" y1="2384" y2="2384" x1="1920" />
            <wire x2="2240" y1="1792" y2="1792" x1="1904" />
            <wire x2="3552" y1="1792" y2="1792" x1="2240" />
            <wire x2="3552" y1="1792" y2="1920" x1="3552" />
            <wire x2="3552" y1="1920" y2="2016" x1="3552" />
            <wire x2="3552" y1="2016" y2="2032" x1="3552" />
            <wire x2="3664" y1="1920" y2="1920" x1="3552" />
            <wire x2="2608" y1="1280" y2="1280" x1="2240" />
            <wire x2="2240" y1="1280" y2="1792" x1="2240" />
            <wire x2="3552" y1="2032" y2="2032" x1="3472" />
        </branch>
        <iomarker fontsize="28" x="3664" y="1920" name="OUTT(15:0)" orien="R0" />
    </sheet>
</drawing>