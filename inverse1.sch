<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="INPUT(15:0)" />
        <signal name="XLXN_6(15:0)" />
        <signal name="XLXN_7" />
        <signal name="OUTPUT(15:0)" />
        <signal name="A(15:0)" />
        <signal name="A(0)" />
        <signal name="A(1)" />
        <signal name="A(2)" />
        <signal name="A(3)" />
        <signal name="A(4)" />
        <signal name="A(5)" />
        <signal name="A(6)" />
        <signal name="A(7)" />
        <signal name="A(8)" />
        <signal name="A(9)" />
        <signal name="A(10)" />
        <signal name="A(11)" />
        <signal name="A(12)" />
        <signal name="XLXN_36" />
        <signal name="A(13)" />
        <signal name="XLXN_38" />
        <signal name="A(14)" />
        <signal name="XLXN_40" />
        <signal name="A(15)" />
        <port polarity="Input" name="INPUT(15:0)" />
        <port polarity="Output" name="OUTPUT(15:0)" />
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="inv16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <rect width="64" x="160" y="-44" height="24" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="add16" name="XLXI_1">
            <blockpin signalname="A(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_6(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_7" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="OUTPUT(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="inv16" name="XLXI_2">
            <blockpin signalname="INPUT(15:0)" name="I(15:0)" />
            <blockpin signalname="XLXN_6(15:0)" name="O(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_3">
            <blockpin signalname="XLXN_7" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_12">
            <blockpin signalname="A(0)" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_13">
            <blockpin signalname="A(1)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_14">
            <blockpin signalname="A(2)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_15">
            <blockpin signalname="A(3)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_16">
            <blockpin signalname="A(4)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_17">
            <blockpin signalname="A(5)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_18">
            <blockpin signalname="A(6)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_19">
            <blockpin signalname="A(7)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_20">
            <blockpin signalname="A(8)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="A(9)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_22">
            <blockpin signalname="A(10)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_23">
            <blockpin signalname="A(11)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_24">
            <blockpin signalname="A(12)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_25">
            <blockpin signalname="A(13)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_26">
            <blockpin signalname="A(14)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_27">
            <blockpin signalname="A(15)" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="INPUT(15:0)">
            <wire x2="832" y1="2080" y2="2080" x1="704" />
            <wire x2="832" y1="2080" y2="2432" x1="832" />
            <wire x2="1072" y1="2432" y2="2432" x1="832" />
        </branch>
        <branch name="XLXN_6(15:0)">
            <wire x2="1424" y1="2432" y2="2432" x1="1296" />
            <wire x2="1424" y1="2400" y2="2432" x1="1424" />
            <wire x2="1552" y1="2400" y2="2400" x1="1424" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1424" y1="1872" y2="1952" x1="1424" />
            <wire x2="1552" y1="1872" y2="1872" x1="1424" />
            <wire x2="1552" y1="1872" y2="2144" x1="1552" />
        </branch>
        <branch name="OUTPUT(15:0)">
            <wire x2="2784" y1="2336" y2="2336" x1="2000" />
            <wire x2="2784" y1="2288" y2="2336" x1="2784" />
        </branch>
        <instance x="1552" y="2592" name="XLXI_1" orien="R0" />
        <instance x="1072" y="2464" name="XLXI_2" orien="R0" />
        <instance x="1360" y="2080" name="XLXI_3" orien="R0" />
        <iomarker fontsize="28" x="704" y="2080" name="INPUT(15:0)" orien="R180" />
        <iomarker fontsize="28" x="2784" y="2288" name="OUTPUT(15:0)" orien="R270" />
        <branch name="A(15:0)">
            <wire x2="1168" y1="192" y2="240" x1="1168" />
            <wire x2="1168" y1="240" y2="304" x1="1168" />
            <wire x2="1168" y1="304" y2="384" x1="1168" />
            <wire x2="1168" y1="384" y2="448" x1="1168" />
            <wire x2="1168" y1="448" y2="544" x1="1168" />
            <wire x2="1168" y1="544" y2="624" x1="1168" />
            <wire x2="1168" y1="624" y2="720" x1="1168" />
            <wire x2="1168" y1="720" y2="800" x1="1168" />
            <wire x2="1168" y1="800" y2="880" x1="1168" />
            <wire x2="1168" y1="880" y2="976" x1="1168" />
            <wire x2="1168" y1="976" y2="1088" x1="1168" />
            <wire x2="1168" y1="1088" y2="1168" x1="1168" />
            <wire x2="1168" y1="1168" y2="1264" x1="1168" />
            <wire x2="1168" y1="1264" y2="1360" x1="1168" />
            <wire x2="1168" y1="1360" y2="1440" x1="1168" />
            <wire x2="1168" y1="1440" y2="1552" x1="1168" />
            <wire x2="1168" y1="1552" y2="2272" x1="1168" />
            <wire x2="1552" y1="2272" y2="2272" x1="1168" />
        </branch>
        <bustap x2="1264" y1="240" y2="240" x1="1168" />
        <bustap x2="1264" y1="304" y2="304" x1="1168" />
        <bustap x2="1264" y1="384" y2="384" x1="1168" />
        <bustap x2="1264" y1="448" y2="448" x1="1168" />
        <bustap x2="1264" y1="544" y2="544" x1="1168" />
        <bustap x2="1264" y1="624" y2="624" x1="1168" />
        <bustap x2="1264" y1="720" y2="720" x1="1168" />
        <bustap x2="1264" y1="800" y2="800" x1="1168" />
        <bustap x2="1264" y1="880" y2="880" x1="1168" />
        <bustap x2="1264" y1="976" y2="976" x1="1168" />
        <bustap x2="1264" y1="1088" y2="1088" x1="1168" />
        <bustap x2="1264" y1="1168" y2="1168" x1="1168" />
        <bustap x2="1264" y1="1264" y2="1264" x1="1168" />
        <bustap x2="1264" y1="1360" y2="1360" x1="1168" />
        <bustap x2="1264" y1="1440" y2="1440" x1="1168" />
        <bustap x2="1264" y1="1552" y2="1552" x1="1168" />
        <instance x="1408" y="1648" name="XLXI_12" orien="R180" />
        <branch name="A(0)">
            <wire x2="1344" y1="1552" y2="1552" x1="1264" />
            <wire x2="1344" y1="1552" y2="1648" x1="1344" />
        </branch>
        <instance x="1472" y="1776" name="XLXI_13" orien="R0" />
        <instance x="1600" y="1776" name="XLXI_14" orien="R0" />
        <instance x="1728" y="1776" name="XLXI_15" orien="R0" />
        <instance x="1840" y="1776" name="XLXI_16" orien="R0" />
        <instance x="1952" y="1776" name="XLXI_17" orien="R0" />
        <instance x="2096" y="1776" name="XLXI_18" orien="R0" />
        <instance x="2240" y="1792" name="XLXI_19" orien="R0" />
        <instance x="2352" y="1776" name="XLXI_20" orien="R0" />
        <instance x="2480" y="1776" name="XLXI_21" orien="R0" />
        <instance x="2592" y="1776" name="XLXI_22" orien="R0" />
        <instance x="2704" y="1776" name="XLXI_23" orien="R0" />
        <instance x="2848" y="1776" name="XLXI_24" orien="R0" />
        <instance x="2976" y="1776" name="XLXI_25" orien="R0" />
        <instance x="3104" y="1776" name="XLXI_26" orien="R0" />
        <instance x="3200" y="1776" name="XLXI_27" orien="R0" />
        <branch name="A(1)">
            <wire x2="1536" y1="1440" y2="1440" x1="1264" />
            <wire x2="1536" y1="1440" y2="1648" x1="1536" />
        </branch>
        <branch name="A(2)">
            <wire x2="1664" y1="1360" y2="1360" x1="1264" />
            <wire x2="1664" y1="1360" y2="1648" x1="1664" />
        </branch>
        <branch name="A(3)">
            <wire x2="1792" y1="1264" y2="1264" x1="1264" />
            <wire x2="1792" y1="1264" y2="1648" x1="1792" />
        </branch>
        <branch name="A(4)">
            <wire x2="1904" y1="1168" y2="1168" x1="1264" />
            <wire x2="1904" y1="1168" y2="1648" x1="1904" />
        </branch>
        <branch name="A(5)">
            <wire x2="2016" y1="1088" y2="1088" x1="1264" />
            <wire x2="2016" y1="1088" y2="1648" x1="2016" />
        </branch>
        <branch name="A(6)">
            <wire x2="2160" y1="976" y2="976" x1="1264" />
            <wire x2="2160" y1="976" y2="1648" x1="2160" />
        </branch>
        <branch name="A(7)">
            <wire x2="2304" y1="880" y2="880" x1="1264" />
            <wire x2="2304" y1="880" y2="1664" x1="2304" />
        </branch>
        <branch name="A(8)">
            <wire x2="2416" y1="800" y2="800" x1="1264" />
            <wire x2="2416" y1="800" y2="1648" x1="2416" />
        </branch>
        <branch name="A(9)">
            <wire x2="2544" y1="720" y2="720" x1="1264" />
            <wire x2="2544" y1="720" y2="1648" x1="2544" />
        </branch>
        <branch name="A(10)">
            <wire x2="2656" y1="624" y2="624" x1="1264" />
            <wire x2="2656" y1="624" y2="1648" x1="2656" />
        </branch>
        <branch name="A(11)">
            <wire x2="2768" y1="544" y2="544" x1="1264" />
            <wire x2="2768" y1="544" y2="1648" x1="2768" />
        </branch>
        <branch name="A(12)">
            <wire x2="2912" y1="448" y2="448" x1="1264" />
            <wire x2="2912" y1="448" y2="1648" x1="2912" />
        </branch>
        <branch name="A(13)">
            <wire x2="2992" y1="384" y2="384" x1="1264" />
            <wire x2="2992" y1="384" y2="416" x1="2992" />
            <wire x2="3040" y1="416" y2="416" x1="2992" />
            <wire x2="3040" y1="416" y2="1648" x1="3040" />
        </branch>
        <branch name="A(14)">
            <wire x2="3104" y1="304" y2="304" x1="1264" />
            <wire x2="3104" y1="304" y2="352" x1="3104" />
            <wire x2="3168" y1="352" y2="352" x1="3104" />
            <wire x2="3168" y1="352" y2="1648" x1="3168" />
        </branch>
        <branch name="A(15)">
            <wire x2="3248" y1="240" y2="240" x1="1264" />
            <wire x2="3248" y1="240" y2="272" x1="3248" />
            <wire x2="3264" y1="272" y2="272" x1="3248" />
            <wire x2="3264" y1="272" y2="1648" x1="3264" />
        </branch>
    </sheet>
</drawing>