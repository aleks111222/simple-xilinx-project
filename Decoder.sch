<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="OP1" />
        <signal name="OP2" />
        <signal name="OP3" />
        <signal name="OP4" />
        <signal name="OP5" />
        <signal name="OP6" />
        <signal name="OP7" />
        <signal name="OP9" />
        <signal name="OP10" />
        <signal name="UceM" />
        <signal name="UclrM" />
        <signal name="ceX" />
        <signal name="RDY" />
        <signal name="RES_RDY" />
        <signal name="OP8" />
        <signal name="UclrTinit" />
        <signal name="UclrT" />
        <signal name="UclrZ" />
        <signal name="UclrSUMM" />
        <signal name="clrT" />
        <signal name="clrZ" />
        <signal name="clrY" />
        <signal name="clrX" />
        <signal name="UceTinit" />
        <signal name="UleT" />
        <signal name="ceT" />
        <signal name="UleZ" />
        <signal name="UceSUMM" />
        <signal name="UceT" />
        <signal name="UceZ" />
        <signal name="ceZ" />
        <signal name="OP12" />
        <signal name="OP11" />
        <signal name="ceY" />
        <signal name="XLXN_169" />
        <port polarity="Input" name="OP1" />
        <port polarity="Input" name="OP2" />
        <port polarity="Input" name="OP3" />
        <port polarity="Input" name="OP4" />
        <port polarity="Input" name="OP5" />
        <port polarity="Input" name="OP6" />
        <port polarity="Input" name="OP7" />
        <port polarity="Input" name="OP9" />
        <port polarity="Input" name="OP10" />
        <port polarity="Output" name="UceM" />
        <port polarity="Output" name="UclrM" />
        <port polarity="Output" name="ceX" />
        <port polarity="Output" name="RDY" />
        <port polarity="Output" name="RES_RDY" />
        <port polarity="Input" name="OP8" />
        <port polarity="Output" name="UclrTinit" />
        <port polarity="Output" name="UclrT" />
        <port polarity="Output" name="UclrZ" />
        <port polarity="Output" name="UclrSUMM" />
        <port polarity="Output" name="clrT" />
        <port polarity="Output" name="clrZ" />
        <port polarity="Output" name="clrY" />
        <port polarity="Output" name="clrX" />
        <port polarity="Output" name="UceTinit" />
        <port polarity="Output" name="UleT" />
        <port polarity="Output" name="ceT" />
        <port polarity="Output" name="UleZ" />
        <port polarity="Output" name="UceSUMM" />
        <port polarity="Output" name="UceT" />
        <port polarity="Output" name="UceZ" />
        <port polarity="Output" name="ceZ" />
        <port polarity="Input" name="OP12" />
        <port polarity="Input" name="OP11" />
        <port polarity="Output" name="ceY" />
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="buf" name="XLXI_2">
            <blockpin signalname="OP8" name="I" />
            <blockpin signalname="UceM" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_13">
            <blockpin signalname="OP11" name="I0" />
            <blockpin signalname="OP9" name="I1" />
            <blockpin signalname="OP4" name="I2" />
            <blockpin signalname="OP2" name="I3" />
            <blockpin signalname="RDY" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_14">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="UclrM" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_15">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="UclrTinit" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_16">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="UclrT" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_18">
            <blockpin signalname="OP1" name="I" />
            <blockpin signalname="UclrSUMM" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_19">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="clrT" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_20">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="clrZ" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_21">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="clrY" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_22">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="clrX" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_23">
            <blockpin signalname="OP2" name="I" />
            <blockpin signalname="UceTinit" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_24">
            <blockpin signalname="OP2" name="I" />
            <blockpin signalname="UleT" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_25">
            <blockpin signalname="XLXN_169" name="I" />
            <blockpin signalname="ceT" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_26">
            <blockpin signalname="OP4" name="I" />
            <blockpin signalname="UleZ" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_27">
            <blockpin signalname="OP4" name="I" />
            <blockpin signalname="ceZ" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_28">
            <blockpin signalname="OP6" name="I" />
            <blockpin signalname="UceSUMM" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_29">
            <blockpin signalname="OP7" name="I" />
            <blockpin signalname="UceT" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_30">
            <blockpin signalname="OP7" name="I" />
            <blockpin signalname="UceZ" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_31">
            <blockpin signalname="OP9" name="I" />
            <blockpin signalname="ceY" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_32">
            <blockpin signalname="OP11" name="I" />
            <blockpin signalname="ceX" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_33">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="RES_RDY" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_17">
            <blockpin signalname="OP12" name="I" />
            <blockpin signalname="UclrZ" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_34">
            <blockpin signalname="OP2" name="I0" />
            <blockpin signalname="OP1" name="I1" />
            <blockpin signalname="XLXN_169" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="OP3">
            <wire x2="848" y1="640" y2="640" x1="768" />
        </branch>
        <branch name="OP4">
            <wire x2="1312" y1="736" y2="736" x1="768" />
            <wire x2="1312" y1="736" y2="1456" x1="1312" />
            <wire x2="1312" y1="1456" y2="1776" x1="1312" />
            <wire x2="2336" y1="1776" y2="1776" x1="1312" />
            <wire x2="1312" y1="1776" y2="1904" x1="1312" />
            <wire x2="2192" y1="1904" y2="1904" x1="1312" />
            <wire x2="2496" y1="1456" y2="1456" x1="1312" />
        </branch>
        <branch name="OP5">
            <wire x2="848" y1="848" y2="848" x1="768" />
        </branch>
        <branch name="OP6">
            <wire x2="1216" y1="944" y2="944" x1="752" />
            <wire x2="1216" y1="944" y2="2048" x1="1216" />
            <wire x2="2064" y1="2048" y2="2048" x1="1216" />
        </branch>
        <branch name="OP7">
            <wire x2="1104" y1="1040" y2="1040" x1="720" />
            <wire x2="1104" y1="1040" y2="2192" x1="1104" />
            <wire x2="1952" y1="2192" y2="2192" x1="1104" />
            <wire x2="1104" y1="2192" y2="2320" x1="1104" />
            <wire x2="1840" y1="2320" y2="2320" x1="1104" />
        </branch>
        <branch name="OP10">
            <wire x2="864" y1="1440" y2="1440" x1="736" />
        </branch>
        <iomarker fontsize="28" x="752" y="464" name="OP1" orien="R180" />
        <iomarker fontsize="28" x="752" y="560" name="OP2" orien="R180" />
        <iomarker fontsize="28" x="768" y="640" name="OP3" orien="R180" />
        <iomarker fontsize="28" x="768" y="736" name="OP4" orien="R180" />
        <iomarker fontsize="28" x="768" y="848" name="OP5" orien="R180" />
        <iomarker fontsize="28" x="752" y="944" name="OP6" orien="R180" />
        <iomarker fontsize="28" x="720" y="1040" name="OP7" orien="R180" />
        <iomarker fontsize="28" x="736" y="1296" name="OP9" orien="R180" />
        <iomarker fontsize="28" x="736" y="1440" name="OP10" orien="R180" />
        <branch name="UclrM">
            <wire x2="2784" y1="256" y2="256" x1="2608" />
        </branch>
        <iomarker fontsize="28" x="2784" y="256" name="UclrM" orien="R0" />
        <branch name="ceX">
            <wire x2="1952" y1="2544" y2="2544" x1="1536" />
            <wire x2="1952" y1="2544" y2="2608" x1="1952" />
            <wire x2="2080" y1="2608" y2="2608" x1="1952" />
        </branch>
        <iomarker fontsize="28" x="2080" y="2608" name="ceX" orien="R0" />
        <branch name="RDY">
            <wire x2="2976" y1="1488" y2="1488" x1="2752" />
        </branch>
        <branch name="RES_RDY">
            <wire x2="1552" y1="2640" y2="2640" x1="1168" />
            <wire x2="1552" y1="2640" y2="2656" x1="1552" />
            <wire x2="1680" y1="2656" y2="2656" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1680" y="2656" name="RES_RDY" orien="R0" />
        <branch name="OP8">
            <wire x2="2544" y1="144" y2="144" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2288" y="144" name="OP8" orien="R180" />
        <iomarker fontsize="28" x="2928" y="144" name="UceM" orien="R0" />
        <branch name="UceM">
            <wire x2="2928" y1="144" y2="144" x1="2768" />
        </branch>
        <instance x="2544" y="176" name="XLXI_2" orien="R0" />
        <instance x="2384" y="288" name="XLXI_14" orien="R0" />
        <instance x="2208" y="400" name="XLXI_15" orien="R0" />
        <instance x="2064" y="544" name="XLXI_16" orien="R0" />
        <instance x="2464" y="768" name="XLXI_18" orien="R0" />
        <instance x="2160" y="848" name="XLXI_19" orien="R0" />
        <instance x="2000" y="912" name="XLXI_20" orien="R0" />
        <instance x="1920" y="1008" name="XLXI_21" orien="R0" />
        <instance x="1808" y="1120" name="XLXI_22" orien="R0" />
        <instance x="1696" y="1232" name="XLXI_23" orien="R0" />
        <instance x="1648" y="1344" name="XLXI_24" orien="R0" />
        <instance x="2336" y="1808" name="XLXI_26" orien="R0" />
        <instance x="2192" y="1936" name="XLXI_27" orien="R0" />
        <instance x="2064" y="2080" name="XLXI_28" orien="R0" />
        <instance x="1952" y="2224" name="XLXI_29" orien="R0" />
        <instance x="1840" y="2352" name="XLXI_30" orien="R0" />
        <instance x="1664" y="2496" name="XLXI_31" orien="R0" />
        <instance x="1312" y="2576" name="XLXI_32" orien="R0" />
        <instance x="944" y="2672" name="XLXI_33" orien="R0" />
        <branch name="UclrTinit">
            <wire x2="2720" y1="368" y2="368" x1="2432" />
        </branch>
        <iomarker fontsize="28" x="2720" y="368" name="UclrTinit" orien="R0" />
        <branch name="UclrT">
            <wire x2="2688" y1="512" y2="512" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2688" y="512" name="UclrT" orien="R0" />
        <branch name="UclrZ">
            <wire x2="2464" y1="640" y2="640" x1="2176" />
        </branch>
        <iomarker fontsize="28" x="2464" y="640" name="UclrZ" orien="R0" />
        <branch name="UclrSUMM">
            <wire x2="2864" y1="736" y2="736" x1="2688" />
        </branch>
        <iomarker fontsize="28" x="2864" y="736" name="UclrSUMM" orien="R0" />
        <branch name="clrT">
            <wire x2="2400" y1="816" y2="816" x1="2384" />
            <wire x2="2400" y1="816" y2="848" x1="2400" />
            <wire x2="2672" y1="848" y2="848" x1="2400" />
        </branch>
        <iomarker fontsize="28" x="2672" y="848" name="clrT" orien="R0" />
        <branch name="clrZ">
            <wire x2="2336" y1="880" y2="880" x1="2224" />
            <wire x2="2336" y1="880" y2="944" x1="2336" />
            <wire x2="2560" y1="944" y2="944" x1="2336" />
        </branch>
        <iomarker fontsize="28" x="2560" y="944" name="clrZ" orien="R0" />
        <branch name="clrY">
            <wire x2="2160" y1="976" y2="976" x1="2144" />
            <wire x2="2160" y1="976" y2="1040" x1="2160" />
            <wire x2="2496" y1="1040" y2="1040" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2496" y="1040" name="clrY" orien="R0" />
        <branch name="clrX">
            <wire x2="2048" y1="1088" y2="1088" x1="2032" />
            <wire x2="2048" y1="1088" y2="1152" x1="2048" />
            <wire x2="2352" y1="1152" y2="1152" x1="2048" />
        </branch>
        <iomarker fontsize="28" x="2352" y="1152" name="clrX" orien="R0" />
        <branch name="UceTinit">
            <wire x2="2112" y1="1200" y2="1200" x1="1920" />
            <wire x2="2112" y1="1200" y2="1248" x1="2112" />
            <wire x2="2208" y1="1248" y2="1248" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="2208" y="1248" name="UceTinit" orien="R0" />
        <branch name="UleT">
            <wire x2="1888" y1="1312" y2="1312" x1="1872" />
            <wire x2="1888" y1="1312" y2="1328" x1="1888" />
            <wire x2="2000" y1="1328" y2="1328" x1="1888" />
        </branch>
        <instance x="2416" y="1712" name="XLXI_25" orien="R0" />
        <instance x="2496" y="1648" name="XLXI_13" orien="R0" />
        <iomarker fontsize="28" x="2976" y="1488" name="RDY" orien="R0" />
        <branch name="ceT">
            <wire x2="2784" y1="1680" y2="1680" x1="2640" />
            <wire x2="2784" y1="1680" y2="1696" x1="2784" />
            <wire x2="2912" y1="1696" y2="1696" x1="2784" />
        </branch>
        <iomarker fontsize="28" x="2912" y="1696" name="ceT" orien="R0" />
        <iomarker fontsize="28" x="2000" y="1328" name="UleT" orien="R0" />
        <branch name="UleZ">
            <wire x2="2688" y1="1776" y2="1776" x1="2560" />
            <wire x2="2688" y1="1776" y2="1808" x1="2688" />
            <wire x2="2784" y1="1808" y2="1808" x1="2688" />
        </branch>
        <iomarker fontsize="28" x="2784" y="1808" name="UleZ" orien="R0" />
        <branch name="UceSUMM">
            <wire x2="2400" y1="2048" y2="2048" x1="2288" />
            <wire x2="2400" y1="2048" y2="2064" x1="2400" />
            <wire x2="2528" y1="2064" y2="2064" x1="2400" />
        </branch>
        <iomarker fontsize="28" x="2528" y="2064" name="UceSUMM" orien="R0" />
        <branch name="UceT">
            <wire x2="2336" y1="2192" y2="2192" x1="2176" />
            <wire x2="2336" y1="2192" y2="2208" x1="2336" />
            <wire x2="2480" y1="2208" y2="2208" x1="2336" />
        </branch>
        <iomarker fontsize="28" x="2480" y="2208" name="UceT" orien="R0" />
        <branch name="UceZ">
            <wire x2="2336" y1="2320" y2="2320" x1="2064" />
        </branch>
        <branch name="ceZ">
            <wire x2="2576" y1="1904" y2="1904" x1="2416" />
            <wire x2="2576" y1="1904" y2="1920" x1="2576" />
            <wire x2="2672" y1="1920" y2="1920" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2672" y="1920" name="ceZ" orien="R0" />
        <iomarker fontsize="28" x="2336" y="2320" name="UceZ" orien="R0" />
        <branch name="OP12">
            <wire x2="624" y1="1664" y2="1664" x1="560" />
            <wire x2="624" y1="1664" y2="2640" x1="624" />
            <wire x2="944" y1="2640" y2="2640" x1="624" />
        </branch>
        <iomarker fontsize="28" x="560" y="1664" name="OP12" orien="R180" />
        <branch name="OP11">
            <wire x2="768" y1="1552" y2="1552" x1="656" />
            <wire x2="768" y1="1536" y2="1552" x1="768" />
            <wire x2="912" y1="1536" y2="1536" x1="768" />
            <wire x2="912" y1="1536" y2="1584" x1="912" />
            <wire x2="912" y1="1584" y2="2544" x1="912" />
            <wire x2="1312" y1="2544" y2="2544" x1="912" />
            <wire x2="2496" y1="1584" y2="1584" x1="912" />
        </branch>
        <iomarker fontsize="28" x="656" y="1552" name="OP11" orien="R180" />
        <branch name="OP9">
            <wire x2="1024" y1="1296" y2="1296" x1="736" />
            <wire x2="1024" y1="1296" y2="1520" x1="1024" />
            <wire x2="2496" y1="1520" y2="1520" x1="1024" />
            <wire x2="1024" y1="1520" y2="2464" x1="1024" />
            <wire x2="1664" y1="2464" y2="2464" x1="1024" />
        </branch>
        <branch name="ceY">
            <wire x2="2080" y1="2464" y2="2464" x1="1888" />
            <wire x2="2080" y1="2464" y2="2480" x1="2080" />
            <wire x2="2112" y1="2480" y2="2480" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="2112" y="2480" name="ceY" orien="R0" />
        <instance x="1952" y="672" name="XLXI_17" orien="R0" />
        <branch name="OP1">
            <wire x2="832" y1="464" y2="464" x1="752" />
            <wire x2="832" y1="256" y2="464" x1="832" />
            <wire x2="1760" y1="256" y2="256" x1="832" />
            <wire x2="1760" y1="256" y2="736" x1="1760" />
            <wire x2="2464" y1="736" y2="736" x1="1760" />
        </branch>
        <branch name="OP2">
            <wire x2="1424" y1="560" y2="560" x1="752" />
            <wire x2="1424" y1="560" y2="1200" x1="1424" />
            <wire x2="1488" y1="1200" y2="1200" x1="1424" />
            <wire x2="1696" y1="1200" y2="1200" x1="1488" />
            <wire x2="1488" y1="1200" y2="1312" x1="1488" />
            <wire x2="1648" y1="1312" y2="1312" x1="1488" />
            <wire x2="1424" y1="1200" y2="1408" x1="1424" />
            <wire x2="2304" y1="1408" y2="1408" x1="1424" />
            <wire x2="2496" y1="1392" y2="1392" x1="2304" />
            <wire x2="2304" y1="1392" y2="1408" x1="2304" />
        </branch>
        <instance x="2128" y="1776" name="XLXI_34" orien="R0" />
        <branch name="XLXN_169">
            <wire x2="2416" y1="1680" y2="1680" x1="2384" />
        </branch>
        <branch name="OP1">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="1648" type="branch" />
            <wire x2="2128" y1="1648" y2="1648" x1="1984" />
        </branch>
        <branch name="OP2">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1984" y="1712" type="branch" />
            <wire x2="2128" y1="1712" y2="1712" x1="1984" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2240" y="256" type="branch" />
            <wire x2="2384" y1="256" y2="256" x1="2240" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2096" y="368" type="branch" />
            <wire x2="2208" y1="368" y2="368" x1="2096" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="512" type="branch" />
            <wire x2="2064" y1="512" y2="512" x1="2000" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1888" y="640" type="branch" />
            <wire x2="1952" y1="640" y2="640" x1="1888" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2080" y="816" type="branch" />
            <wire x2="2160" y1="816" y2="816" x1="2080" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="880" type="branch" />
            <wire x2="2000" y1="880" y2="880" x1="1904" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1808" y="976" type="branch" />
            <wire x2="1808" y1="976" y2="976" x1="1792" />
            <wire x2="1920" y1="976" y2="976" x1="1808" />
        </branch>
        <branch name="OP12">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1728" y="1088" type="branch" />
            <wire x2="1728" y1="1088" y2="1088" x1="1712" />
            <wire x2="1808" y1="1088" y2="1088" x1="1728" />
        </branch>
    </sheet>
</drawing>