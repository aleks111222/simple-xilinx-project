-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
          COMPONENT INV
          PORT(
             
						INPUT : IN std_logic_vector(15 downto 0);       
                  OUTPUT: OUT std_logic_vector(15 downto 0);
						Const1 : IN std_logic_vector(15 downto 0)
                  );
          END COMPONENT;

          SIGNAL INPUT :  std_logic_vector(15 downto 0);
			 SIGNAL Const1 :  std_logic_vector(15 downto 0);
			 SIGNAL OUTPUT :  std_logic_vector(15 downto 0);
          

  BEGIN

  -- Component Instantiation
          uut: INV PORT MAP(
                  INPUT => INPUT,
                  OUTPUT => OUTPUT,
						Const1 => Const1
          );


  --  Test Bench Statements
     tb : PROCESS
     BEGIN
	  
	  Const1 <= "0000000000000001";
	  
	  

        wait for 100 ns; -- wait until global set/reset completes

        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
