-- Vhdl test bench created from schematic /home/ise/XILINXSHARED/Project7Rebisz/Converter16.sch - Thu Jan 16 12:10:29 2020
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY Converter16_Converter16_sch_tb IS
END Converter16_Converter16_sch_tb;
ARCHITECTURE behavioral OF Converter16_Converter16_sch_tb IS 

   COMPONENT Converter16
   PORT( INPUT	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          OUTPUT	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0));
   END COMPONENT;

   SIGNAL INPUT	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL OUTPUT	:	STD_LOGIC_VECTOR (15 DOWNTO 0);

BEGIN

   UUT: Converter16 PORT MAP(
		INPUT => INPUT, 
		OUTPUT => OUTPUT
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	
	INPUT <= "10001111";
	
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
