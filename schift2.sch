<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="INPUT(7:0)" />
        <signal name="INPUT(7)" />
        <signal name="INPUT(6)" />
        <signal name="INPUT(5)" />
        <signal name="INPUT(4)" />
        <signal name="INPUT(3)" />
        <signal name="INPUT(2)" />
        <signal name="INPUT(1)" />
        <signal name="OUTPUT(15:0)" />
        <signal name="XLXN_14" />
        <signal name="OUTPUT(6)" />
        <signal name="OUTPUT(5)" />
        <signal name="OUTPUT(4)" />
        <signal name="OUTPUT(3)" />
        <signal name="OUTPUT(2)" />
        <signal name="OUTPUT(1)" />
        <signal name="OUTPUT(0)" />
        <signal name="OUTPUT(7)" />
        <signal name="OUTPUT(8)" />
        <signal name="OUTPUT(9)" />
        <signal name="OUTPUT(10)" />
        <signal name="OUTPUT(11)" />
        <signal name="OUTPUT(12)" />
        <signal name="OUTPUT(13)" />
        <signal name="OUTPUT(14)" />
        <signal name="OUTPUT(15)" />
        <port polarity="Input" name="INPUT(7:0)" />
        <port polarity="Output" name="OUTPUT(15:0)" />
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="buf" name="XLXI_1">
            <blockpin signalname="INPUT(7)" name="I" />
            <blockpin signalname="OUTPUT(6)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_2">
            <blockpin signalname="INPUT(6)" name="I" />
            <blockpin signalname="OUTPUT(5)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_3">
            <blockpin signalname="INPUT(5)" name="I" />
            <blockpin signalname="OUTPUT(4)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_4">
            <blockpin signalname="INPUT(4)" name="I" />
            <blockpin signalname="OUTPUT(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_5">
            <blockpin signalname="INPUT(3)" name="I" />
            <blockpin signalname="OUTPUT(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_6">
            <blockpin signalname="INPUT(2)" name="I" />
            <blockpin signalname="OUTPUT(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_7">
            <blockpin signalname="INPUT(1)" name="I" />
            <blockpin signalname="OUTPUT(0)" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_9">
            <blockpin signalname="OUTPUT(7)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_10">
            <blockpin signalname="OUTPUT(8)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_11">
            <blockpin signalname="OUTPUT(9)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="OUTPUT(10)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_13">
            <blockpin signalname="OUTPUT(11)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_14">
            <blockpin signalname="OUTPUT(12)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_15">
            <blockpin signalname="OUTPUT(13)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_16">
            <blockpin signalname="OUTPUT(14)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_17">
            <blockpin signalname="OUTPUT(15)" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="INPUT(7:0)">
            <wire x2="704" y1="320" y2="320" x1="448" />
            <wire x2="704" y1="320" y2="384" x1="704" />
            <wire x2="704" y1="384" y2="480" x1="704" />
            <wire x2="704" y1="480" y2="576" x1="704" />
            <wire x2="704" y1="576" y2="704" x1="704" />
            <wire x2="704" y1="704" y2="800" x1="704" />
            <wire x2="704" y1="800" y2="912" x1="704" />
            <wire x2="704" y1="912" y2="1008" x1="704" />
            <wire x2="704" y1="1008" y2="1072" x1="704" />
        </branch>
        <iomarker fontsize="28" x="448" y="320" name="INPUT(7:0)" orien="R180" />
        <bustap x2="800" y1="384" y2="384" x1="704" />
        <bustap x2="800" y1="480" y2="480" x1="704" />
        <bustap x2="800" y1="576" y2="576" x1="704" />
        <bustap x2="800" y1="704" y2="704" x1="704" />
        <bustap x2="800" y1="800" y2="800" x1="704" />
        <bustap x2="800" y1="912" y2="912" x1="704" />
        <bustap x2="800" y1="1008" y2="1008" x1="704" />
        <instance x="864" y="416" name="XLXI_1" orien="R0" />
        <instance x="864" y="512" name="XLXI_2" orien="R0" />
        <instance x="864" y="624" name="XLXI_3" orien="R0" />
        <instance x="864" y="736" name="XLXI_4" orien="R0" />
        <instance x="864" y="848" name="XLXI_5" orien="R0" />
        <instance x="864" y="944" name="XLXI_6" orien="R0" />
        <instance x="864" y="1056" name="XLXI_7" orien="R0" />
        <branch name="INPUT(7)">
            <wire x2="864" y1="384" y2="384" x1="800" />
        </branch>
        <branch name="INPUT(6)">
            <wire x2="864" y1="480" y2="480" x1="800" />
        </branch>
        <branch name="INPUT(5)">
            <wire x2="832" y1="576" y2="576" x1="800" />
            <wire x2="832" y1="576" y2="592" x1="832" />
            <wire x2="864" y1="592" y2="592" x1="832" />
        </branch>
        <branch name="INPUT(4)">
            <wire x2="864" y1="704" y2="704" x1="800" />
        </branch>
        <branch name="INPUT(3)">
            <wire x2="832" y1="800" y2="800" x1="800" />
            <wire x2="832" y1="800" y2="816" x1="832" />
            <wire x2="864" y1="816" y2="816" x1="832" />
        </branch>
        <branch name="INPUT(2)">
            <wire x2="864" y1="912" y2="912" x1="800" />
        </branch>
        <branch name="INPUT(1)">
            <wire x2="832" y1="1008" y2="1008" x1="800" />
            <wire x2="832" y1="1008" y2="1024" x1="832" />
            <wire x2="864" y1="1024" y2="1024" x1="832" />
        </branch>
        <iomarker fontsize="28" x="2416" y="304" name="OUTPUT(15:0)" orien="R0" />
        <branch name="OUTPUT(15:0)">
            <wire x2="2416" y1="304" y2="304" x1="2016" />
            <wire x2="2016" y1="304" y2="384" x1="2016" />
            <wire x2="2016" y1="384" y2="448" x1="2016" />
            <wire x2="2016" y1="448" y2="528" x1="2016" />
            <wire x2="2016" y1="528" y2="624" x1="2016" />
            <wire x2="2016" y1="624" y2="688" x1="2016" />
            <wire x2="2016" y1="688" y2="800" x1="2016" />
            <wire x2="2016" y1="800" y2="864" x1="2016" />
            <wire x2="2016" y1="864" y2="960" x1="2016" />
            <wire x2="2016" y1="960" y2="1056" x1="2016" />
            <wire x2="2016" y1="1056" y2="1136" x1="2016" />
            <wire x2="2016" y1="1136" y2="1232" x1="2016" />
            <wire x2="2016" y1="1232" y2="1328" x1="2016" />
            <wire x2="2016" y1="1328" y2="1440" x1="2016" />
            <wire x2="2016" y1="1440" y2="1520" x1="2016" />
            <wire x2="2016" y1="1520" y2="1648" x1="2016" />
            <wire x2="2016" y1="1648" y2="1760" x1="2016" />
            <wire x2="2016" y1="1760" y2="2080" x1="2016" />
        </branch>
        <bustap x2="1920" y1="384" y2="384" x1="2016" />
        <bustap x2="1920" y1="448" y2="448" x1="2016" />
        <bustap x2="1920" y1="528" y2="528" x1="2016" />
        <bustap x2="1920" y1="624" y2="624" x1="2016" />
        <bustap x2="1920" y1="688" y2="688" x1="2016" />
        <bustap x2="1920" y1="800" y2="800" x1="2016" />
        <bustap x2="1920" y1="864" y2="864" x1="2016" />
        <bustap x2="1920" y1="960" y2="960" x1="2016" />
        <bustap x2="1920" y1="1056" y2="1056" x1="2016" />
        <bustap x2="1920" y1="1136" y2="1136" x1="2016" />
        <bustap x2="1920" y1="1232" y2="1232" x1="2016" />
        <bustap x2="1920" y1="1328" y2="1328" x1="2016" />
        <bustap x2="1920" y1="1440" y2="1440" x1="2016" />
        <bustap x2="1920" y1="1520" y2="1520" x1="2016" />
        <bustap x2="1920" y1="1648" y2="1648" x1="2016" />
        <bustap x2="1920" y1="1760" y2="1760" x1="2016" />
        <branch name="OUTPUT(6)">
            <wire x2="1920" y1="384" y2="384" x1="1088" />
        </branch>
        <branch name="OUTPUT(5)">
            <wire x2="1504" y1="480" y2="480" x1="1088" />
            <wire x2="1504" y1="448" y2="480" x1="1504" />
            <wire x2="1920" y1="448" y2="448" x1="1504" />
        </branch>
        <branch name="OUTPUT(4)">
            <wire x2="1504" y1="592" y2="592" x1="1088" />
            <wire x2="1504" y1="528" y2="592" x1="1504" />
            <wire x2="1920" y1="528" y2="528" x1="1504" />
        </branch>
        <branch name="OUTPUT(3)">
            <wire x2="1504" y1="704" y2="704" x1="1088" />
            <wire x2="1504" y1="624" y2="704" x1="1504" />
            <wire x2="1920" y1="624" y2="624" x1="1504" />
        </branch>
        <branch name="OUTPUT(2)">
            <wire x2="1520" y1="816" y2="816" x1="1088" />
            <wire x2="1520" y1="688" y2="816" x1="1520" />
            <wire x2="1920" y1="688" y2="688" x1="1520" />
        </branch>
        <branch name="OUTPUT(1)">
            <wire x2="1536" y1="912" y2="912" x1="1088" />
            <wire x2="1536" y1="800" y2="912" x1="1536" />
            <wire x2="1920" y1="800" y2="800" x1="1536" />
        </branch>
        <branch name="OUTPUT(0)">
            <wire x2="1552" y1="1024" y2="1024" x1="1088" />
            <wire x2="1552" y1="864" y2="1024" x1="1552" />
            <wire x2="1920" y1="864" y2="864" x1="1552" />
        </branch>
        <instance x="1632" y="2112" name="XLXI_9" orien="R0" />
        <instance x="1472" y="2112" name="XLXI_10" orien="R0" />
        <instance x="1312" y="2112" name="XLXI_11" orien="R0" />
        <instance x="1136" y="2112" name="XLXI_12" orien="R0" />
        <instance x="944" y="2128" name="XLXI_13" orien="R0" />
        <instance x="752" y="2128" name="XLXI_14" orien="R0" />
        <instance x="576" y="2128" name="XLXI_15" orien="R0" />
        <instance x="384" y="2128" name="XLXI_16" orien="R0" />
        <branch name="OUTPUT(7)">
            <wire x2="1920" y1="1760" y2="1760" x1="1696" />
            <wire x2="1696" y1="1760" y2="1984" x1="1696" />
        </branch>
        <branch name="OUTPUT(8)">
            <wire x2="1920" y1="1648" y2="1648" x1="1536" />
            <wire x2="1536" y1="1648" y2="1984" x1="1536" />
        </branch>
        <branch name="OUTPUT(9)">
            <wire x2="1920" y1="1520" y2="1520" x1="1376" />
            <wire x2="1376" y1="1520" y2="1984" x1="1376" />
        </branch>
        <branch name="OUTPUT(10)">
            <wire x2="1920" y1="1440" y2="1440" x1="1200" />
            <wire x2="1200" y1="1440" y2="1984" x1="1200" />
        </branch>
        <branch name="OUTPUT(11)">
            <wire x2="1920" y1="1328" y2="1328" x1="1008" />
            <wire x2="1008" y1="1328" y2="2000" x1="1008" />
        </branch>
        <branch name="OUTPUT(12)">
            <wire x2="1920" y1="1232" y2="1232" x1="816" />
            <wire x2="816" y1="1232" y2="2000" x1="816" />
        </branch>
        <branch name="OUTPUT(13)">
            <wire x2="1920" y1="1136" y2="1136" x1="640" />
            <wire x2="640" y1="1136" y2="2000" x1="640" />
        </branch>
        <branch name="OUTPUT(14)">
            <wire x2="448" y1="1920" y2="2000" x1="448" />
            <wire x2="1136" y1="1920" y2="1920" x1="448" />
            <wire x2="1136" y1="1056" y2="1920" x1="1136" />
            <wire x2="1920" y1="1056" y2="1056" x1="1136" />
        </branch>
        <instance x="1616" y="1088" name="XLXI_17" orien="R0" />
        <branch name="OUTPUT(15)">
            <wire x2="1680" y1="944" y2="960" x1="1680" />
            <wire x2="1904" y1="944" y2="944" x1="1680" />
            <wire x2="1904" y1="944" y2="960" x1="1904" />
            <wire x2="1920" y1="960" y2="960" x1="1904" />
        </branch>
    </sheet>
</drawing>
