<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="OP1" />
        <signal name="XLXN_10" />
        <signal name="OP2" />
        <signal name="XLXN_121" />
        <signal name="XLXN_127" />
        <signal name="XLXN_136" />
        <signal name="XLXN_148" />
        <signal name="XLXN_150" />
        <signal name="NR" />
        <signal name="XLXN_154" />
        <signal name="XLXN_155" />
        <signal name="OP3" />
        <signal name="XLXN_169" />
        <signal name="OP4" />
        <signal name="XLXN_172" />
        <signal name="XLXN_173" />
        <signal name="XLXN_177" />
        <signal name="XLXN_178" />
        <signal name="T0" />
        <signal name="OP5" />
        <signal name="XLXN_199" />
        <signal name="OP6" />
        <signal name="XLXN_220" />
        <signal name="TB0" />
        <signal name="XLXN_228" />
        <signal name="XLXN_231" />
        <signal name="OP7" />
        <signal name="OP8" />
        <signal name="OP9" />
        <signal name="XLXN_239" />
        <signal name="XLXN_240" />
        <signal name="XLXN_243" />
        <signal name="XLXN_244" />
        <signal name="OP10" />
        <signal name="XLXN_247" />
        <signal name="XLXN_253" />
        <signal name="XLXN_254" />
        <signal name="XLXN_255" />
        <signal name="OP11" />
        <signal name="OP12" />
        <signal name="XLXN_263" />
        <signal name="XLXN_265" />
        <signal name="XLXN_266" />
        <signal name="XLXN_267" />
        <signal name="XLXN_268" />
        <signal name="XLXN_269" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="OP1" />
        <port polarity="Output" name="OP2" />
        <port polarity="Input" name="NR" />
        <port polarity="Output" name="OP3" />
        <port polarity="Output" name="OP4" />
        <port polarity="Input" name="T0" />
        <port polarity="Output" name="OP5" />
        <port polarity="Output" name="OP6" />
        <port polarity="Input" name="TB0" />
        <port polarity="Output" name="OP7" />
        <port polarity="Output" name="OP8" />
        <port polarity="Output" name="OP9" />
        <port polarity="Output" name="OP10" />
        <port polarity="Output" name="OP11" />
        <port polarity="Output" name="OP12" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <blockdef name="ConditionBlock">
            <timestamp>2020-1-16T14:20:24</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <block symbolname="or2" name="XLXI_1">
            <blockpin signalname="XLXN_10" name="I0" />
            <blockpin signalname="OP12" name="I1" />
            <blockpin signalname="XLXN_150" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_2">
            <attr value="1" name="INIT">
                <trait verilog="all:0 dp:1" />
                <trait vhdl="all:0 gm:1" />
                <trait valuetype="Bit" />
            </attr>
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_150" name="D" />
            <blockpin signalname="OP1" name="Q" />
        </block>
        <block symbolname="or2" name="XLXI_8">
            <blockpin signalname="XLXN_155" name="I0" />
            <blockpin signalname="XLXN_154" name="I1" />
            <blockpin signalname="XLXN_148" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_9">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_148" name="D" />
            <blockpin signalname="OP2" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_50">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_121" name="D" />
            <blockpin signalname="OP8" name="Q" />
        </block>
        <block symbolname="or2" name="XLXI_52">
            <blockpin signalname="XLXN_127" name="I0" />
            <blockpin signalname="XLXN_268" name="I1" />
            <blockpin signalname="XLXN_121" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_53">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_136" name="D" />
            <blockpin signalname="OP9" name="Q" />
        </block>
        <block symbolname="or2" name="XLXI_55">
            <blockpin signalname="XLXN_239" name="I0" />
            <blockpin signalname="XLXN_240" name="I1" />
            <blockpin signalname="XLXN_136" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_56">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_243" name="D" />
            <blockpin signalname="OP10" name="Q" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_58">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP1" name="OP" />
            <blockpin signalname="XLXN_155" name="OUT1" />
            <blockpin signalname="XLXN_10" name="OUT0" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_59">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP2" name="OP" />
            <blockpin signalname="XLXN_154" name="OUT1" />
            <blockpin signalname="XLXN_267" name="OUT0" />
        </block>
        <block symbolname="or2" name="XLXI_65">
            <blockpin signalname="XLXN_169" name="I0" />
            <blockpin signalname="XLXN_267" name="I1" />
            <blockpin signalname="XLXN_173" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_66">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_173" name="D" />
            <blockpin signalname="OP3" name="Q" />
        </block>
        <block symbolname="or2" name="XLXI_67">
            <blockpin signalname="XLXN_178" name="I0" />
            <blockpin signalname="XLXN_177" name="I1" />
            <blockpin signalname="XLXN_172" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_68">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_172" name="D" />
            <blockpin signalname="OP4" name="Q" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_69">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP3" name="OP" />
            <blockpin signalname="XLXN_178" name="OUT1" />
            <blockpin signalname="XLXN_169" name="OUT0" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_70">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP4" name="OP" />
            <blockpin signalname="XLXN_177" name="OUT1" />
            <blockpin signalname="XLXN_268" name="OUT0" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_60">
            <blockpin signalname="T0" name="COND" />
            <blockpin signalname="OP5" name="OP" />
            <blockpin signalname="XLXN_263" name="OUT1" />
            <blockpin signalname="XLXN_220" name="OUT0" />
        </block>
        <block symbolname="fd" name="XLXI_73">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_199" name="D" />
            <blockpin signalname="OP5" name="Q" />
        </block>
        <block symbolname="or2" name="XLXI_74">
            <blockpin signalname="OP7" name="I0" />
            <blockpin signalname="XLXN_269" name="I1" />
            <blockpin signalname="XLXN_199" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_27">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_228" name="D" />
            <blockpin signalname="OP6" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_79">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_231" name="D" />
            <blockpin signalname="OP7" name="Q" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_80">
            <blockpin signalname="TB0" name="COND" />
            <blockpin signalname="XLXN_220" name="OP" />
            <blockpin signalname="XLXN_228" name="OUT1" />
            <blockpin signalname="XLXN_231" name="OUT0" />
        </block>
        <block symbolname="buf" name="XLXI_81">
            <blockpin signalname="OP6" name="I" />
            <blockpin signalname="XLXN_231" name="O" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_82">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP8" name="OP" />
            <blockpin signalname="XLXN_239" name="OUT1" />
            <blockpin signalname="XLXN_127" name="OUT0" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_83">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP9" name="OP" />
            <blockpin signalname="XLXN_240" name="OUT1" />
            <blockpin signalname="XLXN_244" name="OUT0" />
        </block>
        <block symbolname="or2" name="XLXI_84">
            <blockpin signalname="XLXN_247" name="I0" />
            <blockpin signalname="XLXN_244" name="I1" />
            <blockpin signalname="XLXN_243" name="O" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_85">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP10" name="OP" />
            <blockpin signalname="XLXN_253" name="OUT1" />
            <blockpin signalname="XLXN_247" name="OUT0" />
        </block>
        <block symbolname="fd" name="XLXI_86">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_255" name="D" />
            <blockpin signalname="OP11" name="Q" />
        </block>
        <block symbolname="ConditionBlock" name="XLXI_87">
            <blockpin signalname="NR" name="COND" />
            <blockpin signalname="OP11" name="OP" />
            <blockpin signalname="XLXN_254" name="OUT1" />
            <blockpin signalname="XLXN_269" name="OUT0" />
        </block>
        <block symbolname="or2" name="XLXI_88">
            <blockpin signalname="XLXN_253" name="I0" />
            <blockpin signalname="XLXN_254" name="I1" />
            <blockpin signalname="XLXN_255" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_89">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_263" name="D" />
            <blockpin signalname="OP12" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7609" height="5382">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <instance x="240" y="3904" name="XLXI_1" orien="R0" />
        <instance x="688" y="4064" name="XLXI_2" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="INIT" x="0" y="-64" type="instance" />
        </instance>
        <branch name="CLK">
            <wire x2="688" y1="3936" y2="3936" x1="544" />
        </branch>
        <branch name="OP1">
            <wire x2="1200" y1="3808" y2="3808" x1="1072" />
            <wire x2="1472" y1="3808" y2="3808" x1="1200" />
            <wire x2="1200" y1="3632" y2="3808" x1="1200" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="240" y1="3840" y2="3840" x1="224" />
            <wire x2="224" y1="3840" y2="4032" x1="224" />
            <wire x2="1872" y1="4032" y2="4032" x1="224" />
            <wire x2="1872" y1="3808" y2="3808" x1="1856" />
            <wire x2="1872" y1="3808" y2="4032" x1="1872" />
        </branch>
        <instance x="2160" y="3872" name="XLXI_8" orien="R0" />
        <instance x="2608" y="4032" name="XLXI_9" orien="R0" />
        <branch name="OP2">
            <wire x2="3120" y1="3776" y2="3776" x1="2992" />
            <wire x2="3328" y1="3776" y2="3776" x1="3120" />
            <wire x2="3120" y1="3600" y2="3776" x1="3120" />
            <wire x2="3328" y1="3712" y2="3776" x1="3328" />
            <wire x2="3392" y1="3712" y2="3712" x1="3328" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="3904" type="branch" />
            <wire x2="2608" y1="3904" y2="3904" x1="2464" />
        </branch>
        <iomarker fontsize="28" x="544" y="3936" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="1200" y="3632" name="OP1" orien="R270" />
        <iomarker fontsize="28" x="3120" y="3600" name="OP2" orien="R270" />
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="560" y="3248" type="branch" />
            <wire x2="576" y1="3248" y2="3248" x1="560" />
            <wire x2="624" y1="3184" y2="3184" x1="576" />
            <wire x2="576" y1="3184" y2="3248" x1="576" />
        </branch>
        <instance x="336" y="3216" name="XLXI_52" orien="R0" />
        <instance x="624" y="3312" name="XLXI_50" orien="R0" />
        <branch name="XLXN_121">
            <wire x2="608" y1="3120" y2="3120" x1="592" />
            <wire x2="624" y1="3056" y2="3056" x1="608" />
            <wire x2="608" y1="3056" y2="3120" x1="608" />
        </branch>
        <branch name="XLXN_127">
            <wire x2="336" y1="3152" y2="3152" x1="320" />
            <wire x2="320" y1="3152" y2="3312" x1="320" />
            <wire x2="1712" y1="3312" y2="3312" x1="320" />
            <wire x2="1712" y1="3104" y2="3104" x1="1488" />
            <wire x2="1712" y1="3104" y2="3312" x1="1712" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1952" y="3184" type="branch" />
            <wire x2="2096" y1="3184" y2="3184" x1="1952" />
        </branch>
        <branch name="XLXN_136">
            <wire x2="2080" y1="2992" y2="2992" x1="2048" />
            <wire x2="2080" y1="2992" y2="3056" x1="2080" />
            <wire x2="2096" y1="3056" y2="3056" x1="2080" />
        </branch>
        <instance x="2096" y="3312" name="XLXI_53" orien="R0" />
        <branch name="CLK">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3408" y="3200" type="branch" />
            <wire x2="3424" y1="3072" y2="3072" x1="3408" />
            <wire x2="3408" y1="3072" y2="3200" x1="3408" />
        </branch>
        <branch name="XLXN_148">
            <wire x2="2608" y1="3776" y2="3776" x1="2416" />
        </branch>
        <branch name="XLXN_150">
            <wire x2="688" y1="3808" y2="3808" x1="496" />
        </branch>
        <instance x="1472" y="3840" name="XLXI_58" orien="R0">
        </instance>
        <branch name="NR">
            <wire x2="1472" y1="3744" y2="3744" x1="1440" />
        </branch>
        <iomarker fontsize="28" x="1440" y="3744" name="NR" orien="R180" />
        <branch name="NR">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3312" y="3648" type="branch" />
            <wire x2="3392" y1="3648" y2="3648" x1="3312" />
        </branch>
        <instance x="3392" y="3744" name="XLXI_59" orien="R0">
        </instance>
        <branch name="XLXN_154">
            <wire x2="2144" y1="3440" y2="3744" x1="2144" />
            <wire x2="2160" y1="3744" y2="3744" x1="2144" />
            <wire x2="3856" y1="3440" y2="3440" x1="2144" />
            <wire x2="3856" y1="3440" y2="3648" x1="3856" />
            <wire x2="3856" y1="3648" y2="3648" x1="3776" />
        </branch>
        <branch name="XLXN_155">
            <wire x2="2000" y1="3744" y2="3744" x1="1856" />
            <wire x2="2000" y1="3744" y2="3808" x1="2000" />
            <wire x2="2160" y1="3808" y2="3808" x1="2000" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="4576" type="branch" />
            <wire x2="1472" y1="4576" y2="4576" x1="1328" />
        </branch>
        <branch name="OP3">
            <wire x2="1984" y1="4448" y2="4448" x1="1856" />
            <wire x2="2256" y1="4448" y2="4448" x1="1984" />
            <wire x2="1984" y1="4336" y2="4448" x1="1984" />
        </branch>
        <branch name="XLXN_169">
            <wire x2="1024" y1="4480" y2="4480" x1="1008" />
            <wire x2="1008" y1="4480" y2="4672" x1="1008" />
            <wire x2="2656" y1="4672" y2="4672" x1="1008" />
            <wire x2="2656" y1="4448" y2="4448" x1="2640" />
            <wire x2="2656" y1="4448" y2="4672" x1="2656" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3248" y="4544" type="branch" />
            <wire x2="3392" y1="4544" y2="4544" x1="3248" />
        </branch>
        <branch name="XLXN_172">
            <wire x2="3392" y1="4416" y2="4416" x1="3200" />
        </branch>
        <branch name="XLXN_173">
            <wire x2="1472" y1="4448" y2="4448" x1="1280" />
        </branch>
        <branch name="NR">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2224" y="4384" type="branch" />
            <wire x2="2256" y1="4384" y2="4384" x1="2224" />
        </branch>
        <branch name="NR">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4096" y="4288" type="branch" />
            <wire x2="4176" y1="4288" y2="4288" x1="4096" />
        </branch>
        <branch name="XLXN_177">
            <wire x2="2928" y1="4080" y2="4384" x1="2928" />
            <wire x2="2944" y1="4384" y2="4384" x1="2928" />
            <wire x2="4592" y1="4080" y2="4080" x1="2928" />
            <wire x2="4592" y1="4080" y2="4288" x1="4592" />
            <wire x2="4592" y1="4288" y2="4288" x1="4560" />
        </branch>
        <branch name="XLXN_178">
            <wire x2="2784" y1="4384" y2="4384" x1="2640" />
            <wire x2="2784" y1="4384" y2="4448" x1="2784" />
            <wire x2="2944" y1="4448" y2="4448" x1="2784" />
        </branch>
        <instance x="1024" y="4544" name="XLXI_65" orien="R0" />
        <instance x="1472" y="4704" name="XLXI_66" orien="R0" />
        <instance x="2944" y="4512" name="XLXI_67" orien="R0" />
        <instance x="3392" y="4672" name="XLXI_68" orien="R0" />
        <instance x="2256" y="4480" name="XLXI_69" orien="R0">
        </instance>
        <instance x="4176" y="4384" name="XLXI_70" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1984" y="4336" name="OP3" orien="R270" />
        <branch name="OP4">
            <wire x2="3904" y1="4416" y2="4416" x1="3776" />
            <wire x2="4112" y1="4416" y2="4416" x1="3904" />
            <wire x2="3904" y1="4320" y2="4416" x1="3904" />
            <wire x2="4112" y1="4352" y2="4416" x1="4112" />
            <wire x2="4176" y1="4352" y2="4352" x1="4112" />
        </branch>
        <iomarker fontsize="28" x="3904" y="4320" name="OP4" orien="R270" />
        <branch name="T0">
            <wire x2="5440" y1="4240" y2="4320" x1="5440" />
            <wire x2="5456" y1="4320" y2="4320" x1="5440" />
            <wire x2="5488" y1="4240" y2="4240" x1="5440" />
            <wire x2="5488" y1="4160" y2="4240" x1="5488" />
        </branch>
        <branch name="OP5">
            <wire x2="5376" y1="4384" y2="4384" x1="5360" />
            <wire x2="5456" y1="4384" y2="4384" x1="5376" />
            <wire x2="5376" y1="4288" y2="4384" x1="5376" />
        </branch>
        <instance x="5456" y="4416" name="XLXI_60" orien="R0">
        </instance>
        <instance x="4976" y="4640" name="XLXI_73" orien="R0" />
        <iomarker fontsize="28" x="5376" y="4288" name="OP5" orien="R270" />
        <iomarker fontsize="28" x="5488" y="4160" name="T0" orien="R270" />
        <instance x="4688" y="4480" name="XLXI_74" orien="R0" />
        <branch name="CLK">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4960" y="5024" type="branch" />
            <wire x2="4992" y1="4944" y2="4944" x1="4944" />
            <wire x2="4944" y1="4944" y2="5024" x1="4944" />
            <wire x2="4960" y1="5024" y2="5024" x1="4944" />
        </branch>
        <branch name="XLXN_199">
            <wire x2="4976" y1="4384" y2="4384" x1="4944" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="4928" y="4512" type="branch" />
            <wire x2="4976" y1="4512" y2="4512" x1="4928" />
        </branch>
        <instance x="4992" y="5072" name="XLXI_27" orien="R0" />
        <iomarker fontsize="28" x="5392" y="5072" name="OP6" orien="R180" />
        <instance x="5984" y="5184" name="XLXI_79" orien="R0" />
        <instance x="6016" y="4576" name="XLXI_80" orien="R0">
        </instance>
        <branch name="XLXN_220">
            <wire x2="5920" y1="4384" y2="4384" x1="5840" />
            <wire x2="5920" y1="4384" y2="4544" x1="5920" />
            <wire x2="6016" y1="4544" y2="4544" x1="5920" />
        </branch>
        <iomarker fontsize="28" x="6032" y="4288" name="TB0" orien="R0" />
        <branch name="TB0">
            <wire x2="6032" y1="4288" y2="4288" x1="6000" />
            <wire x2="6000" y1="4288" y2="4480" x1="6000" />
            <wire x2="6016" y1="4480" y2="4480" x1="6000" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5888" y="5056" type="branch" />
            <wire x2="5984" y1="5056" y2="5056" x1="5888" />
        </branch>
        <iomarker fontsize="28" x="6448" y="5040" name="OP7" orien="R0" />
        <branch name="XLXN_228">
            <wire x2="6544" y1="4688" y2="4688" x1="4960" />
            <wire x2="4960" y1="4688" y2="4816" x1="4960" />
            <wire x2="4992" y1="4816" y2="4816" x1="4960" />
            <wire x2="6544" y1="4480" y2="4480" x1="6400" />
            <wire x2="6544" y1="4480" y2="4688" x1="6544" />
        </branch>
        <branch name="OP6">
            <wire x2="5376" y1="4816" y2="4992" x1="5376" />
            <wire x2="5408" y1="4992" y2="4992" x1="5376" />
            <wire x2="5408" y1="4992" y2="5072" x1="5408" />
            <wire x2="5408" y1="5072" y2="5072" x1="5392" />
            <wire x2="5616" y1="4928" y2="4928" x1="5408" />
            <wire x2="5408" y1="4928" y2="4992" x1="5408" />
        </branch>
        <instance x="5616" y="4960" name="XLXI_81" orien="R0" />
        <branch name="XLXN_231">
            <wire x2="5920" y1="4928" y2="4928" x1="5840" />
            <wire x2="5984" y1="4928" y2="4928" x1="5920" />
            <wire x2="6416" y1="4768" y2="4768" x1="5920" />
            <wire x2="5920" y1="4768" y2="4928" x1="5920" />
            <wire x2="6416" y1="4544" y2="4544" x1="6400" />
            <wire x2="6416" y1="4544" y2="4768" x1="6416" />
        </branch>
        <branch name="OP7">
            <wire x2="4688" y1="4416" y2="4416" x1="4672" />
            <wire x2="4672" y1="4416" y2="5184" x1="4672" />
            <wire x2="6384" y1="5184" y2="5184" x1="4672" />
            <wire x2="6384" y1="4928" y2="4928" x1="6368" />
            <wire x2="6384" y1="4928" y2="5040" x1="6384" />
            <wire x2="6384" y1="5040" y2="5184" x1="6384" />
            <wire x2="6448" y1="5040" y2="5040" x1="6384" />
        </branch>
        <instance x="1104" y="3136" name="XLXI_82" orien="R0">
        </instance>
        <branch name="OP8">
            <wire x2="960" y1="2944" y2="2976" x1="960" />
            <wire x2="1024" y1="2976" y2="2976" x1="960" />
            <wire x2="1024" y1="2976" y2="3056" x1="1024" />
            <wire x2="1024" y1="3056" y2="3104" x1="1024" />
            <wire x2="1104" y1="3104" y2="3104" x1="1024" />
            <wire x2="1024" y1="3056" y2="3056" x1="1008" />
        </branch>
        <branch name="NR">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="1120" y="2912" type="branch" />
            <wire x2="1120" y1="2960" y2="2960" x1="1088" />
            <wire x2="1088" y1="2960" y2="3040" x1="1088" />
            <wire x2="1104" y1="3040" y2="3040" x1="1088" />
            <wire x2="1120" y1="2912" y2="2960" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="960" y="2944" name="OP8" orien="R270" />
        <instance x="2528" y="3088" name="XLXI_83" orien="R0">
        </instance>
        <branch name="OP9">
            <wire x2="2512" y1="3056" y2="3056" x1="2480" />
            <wire x2="2528" y1="3056" y2="3056" x1="2512" />
            <wire x2="2512" y1="3056" y2="3152" x1="2512" />
        </branch>
        <iomarker fontsize="28" x="2512" y="3152" name="OP9" orien="R90" />
        <branch name="NR">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="2512" y="2912" type="branch" />
            <wire x2="2512" y1="2912" y2="2992" x1="2512" />
            <wire x2="2528" y1="2992" y2="2992" x1="2512" />
        </branch>
        <instance x="1792" y="3088" name="XLXI_55" orien="R0" />
        <branch name="XLXN_239">
            <wire x2="1632" y1="3040" y2="3040" x1="1488" />
            <wire x2="1632" y1="3024" y2="3040" x1="1632" />
            <wire x2="1792" y1="3024" y2="3024" x1="1632" />
        </branch>
        <branch name="XLXN_240">
            <wire x2="1776" y1="2816" y2="2960" x1="1776" />
            <wire x2="1792" y1="2960" y2="2960" x1="1776" />
            <wire x2="2928" y1="2816" y2="2816" x1="1776" />
            <wire x2="2928" y1="2816" y2="2992" x1="2928" />
            <wire x2="2928" y1="2992" y2="2992" x1="2912" />
        </branch>
        <instance x="3424" y="3200" name="XLXI_56" orien="R0" />
        <instance x="3072" y="3072" name="XLXI_84" orien="R0" />
        <branch name="XLXN_243">
            <wire x2="3376" y1="2976" y2="2976" x1="3328" />
            <wire x2="3376" y1="2944" y2="2976" x1="3376" />
            <wire x2="3424" y1="2944" y2="2944" x1="3376" />
        </branch>
        <branch name="XLXN_244">
            <wire x2="2992" y1="3056" y2="3056" x1="2912" />
            <wire x2="2992" y1="2944" y2="3056" x1="2992" />
            <wire x2="3072" y1="2944" y2="2944" x1="2992" />
        </branch>
        <instance x="3952" y="2976" name="XLXI_85" orien="R0">
        </instance>
        <branch name="OP10">
            <wire x2="3888" y1="2944" y2="2944" x1="3808" />
            <wire x2="3952" y1="2944" y2="2944" x1="3888" />
            <wire x2="3888" y1="2944" y2="3088" x1="3888" />
        </branch>
        <branch name="XLXN_247">
            <wire x2="3072" y1="3008" y2="3008" x1="3056" />
            <wire x2="3056" y1="3008" y2="3312" x1="3056" />
            <wire x2="4352" y1="3312" y2="3312" x1="3056" />
            <wire x2="4352" y1="2944" y2="2944" x1="4336" />
            <wire x2="4352" y1="2944" y2="3312" x1="4352" />
        </branch>
        <branch name="CLK">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4512" y="3120" type="branch" />
            <wire x2="4736" y1="3008" y2="3008" x1="4496" />
            <wire x2="4496" y1="3008" y2="3120" x1="4496" />
            <wire x2="4512" y1="3120" y2="3120" x1="4496" />
        </branch>
        <instance x="5216" y="2912" name="XLXI_87" orien="R0">
        </instance>
        <instance x="4736" y="3136" name="XLXI_86" orien="R0" />
        <branch name="XLXN_253">
            <wire x2="4416" y1="2880" y2="2880" x1="4336" />
        </branch>
        <instance x="4416" y="2944" name="XLXI_88" orien="R0" />
        <branch name="XLXN_254">
            <wire x2="4400" y1="2576" y2="2816" x1="4400" />
            <wire x2="4416" y1="2816" y2="2816" x1="4400" />
            <wire x2="5696" y1="2576" y2="2576" x1="4400" />
            <wire x2="5696" y1="2576" y2="2816" x1="5696" />
            <wire x2="5696" y1="2816" y2="2816" x1="5600" />
        </branch>
        <branch name="XLXN_255">
            <wire x2="4704" y1="2848" y2="2848" x1="4672" />
            <wire x2="4704" y1="2848" y2="2880" x1="4704" />
            <wire x2="4736" y1="2880" y2="2880" x1="4704" />
        </branch>
        <branch name="OP11">
            <wire x2="5152" y1="2880" y2="2880" x1="5120" />
            <wire x2="5216" y1="2880" y2="2880" x1="5152" />
            <wire x2="5152" y1="2880" y2="3040" x1="5152" />
        </branch>
        <branch name="NR">
            <attrtext style="alignment:SOFT-VLEFT;fontsize:28;fontname:Arial" attrname="Name" x="5168" y="2752" type="branch" />
            <wire x2="5168" y1="2752" y2="2816" x1="5168" />
            <wire x2="5216" y1="2816" y2="2816" x1="5168" />
        </branch>
        <branch name="NR">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="2880" type="branch" />
            <wire x2="3952" y1="2880" y2="2880" x1="3872" />
        </branch>
        <iomarker fontsize="28" x="3888" y="3088" name="OP10" orien="R90" />
        <iomarker fontsize="28" x="5152" y="3040" name="OP11" orien="R90" />
        <instance x="5936" y="3152" name="XLXI_89" orien="R0" />
        <branch name="CLK">
            <attrtext style="alignment:SOFT-VRIGHT;fontsize:28;fontname:Arial" attrname="Name" x="5840" y="3040" type="branch" />
            <wire x2="5936" y1="3024" y2="3024" x1="5840" />
            <wire x2="5840" y1="3024" y2="3040" x1="5840" />
        </branch>
        <branch name="OP12">
            <wire x2="6496" y1="2272" y2="2272" x1="112" />
            <wire x2="6496" y1="2272" y2="2896" x1="6496" />
            <wire x2="112" y1="2272" y2="3776" x1="112" />
            <wire x2="240" y1="3776" y2="3776" x1="112" />
            <wire x2="6400" y1="2896" y2="2896" x1="6320" />
            <wire x2="6496" y1="2896" y2="2896" x1="6400" />
            <wire x2="6400" y1="2896" y2="3040" x1="6400" />
        </branch>
        <iomarker fontsize="28" x="6400" y="3040" name="OP12" orien="R90" />
        <branch name="XLXN_263">
            <wire x2="5744" y1="2896" y2="3424" x1="5744" />
            <wire x2="5888" y1="3424" y2="3424" x1="5744" />
            <wire x2="5888" y1="3424" y2="4320" x1="5888" />
            <wire x2="5936" y1="2896" y2="2896" x1="5744" />
            <wire x2="5888" y1="4320" y2="4320" x1="5840" />
        </branch>
        <branch name="XLXN_267">
            <wire x2="880" y1="4160" y2="4416" x1="880" />
            <wire x2="1024" y1="4416" y2="4416" x1="880" />
            <wire x2="2048" y1="4160" y2="4160" x1="880" />
            <wire x2="3792" y1="4016" y2="4016" x1="2048" />
            <wire x2="2048" y1="4016" y2="4144" x1="2048" />
            <wire x2="2048" y1="4144" y2="4160" x1="2048" />
            <wire x2="3792" y1="3712" y2="3712" x1="3776" />
            <wire x2="3792" y1="3712" y2="4016" x1="3792" />
        </branch>
        <branch name="XLXN_268">
            <wire x2="240" y1="3088" y2="3376" x1="240" />
            <wire x2="2016" y1="3376" y2="3376" x1="240" />
            <wire x2="4752" y1="3376" y2="3376" x1="2016" />
            <wire x2="4752" y1="3376" y2="3392" x1="4752" />
            <wire x2="4752" y1="3392" y2="4192" x1="4752" />
            <wire x2="4752" y1="4192" y2="4256" x1="4752" />
            <wire x2="336" y1="3088" y2="3088" x1="240" />
            <wire x2="4640" y1="4352" y2="4352" x1="4560" />
            <wire x2="4640" y1="4256" y2="4352" x1="4640" />
            <wire x2="4752" y1="4256" y2="4256" x1="4640" />
        </branch>
        <branch name="XLXN_269">
            <wire x2="4688" y1="4304" y2="4304" x1="4672" />
            <wire x2="4880" y1="4304" y2="4304" x1="4688" />
            <wire x2="4672" y1="4304" y2="4352" x1="4672" />
            <wire x2="4688" y1="4352" y2="4352" x1="4672" />
            <wire x2="5600" y1="4000" y2="4000" x1="4880" />
            <wire x2="4880" y1="4000" y2="4304" x1="4880" />
            <wire x2="5664" y1="2880" y2="2880" x1="5600" />
            <wire x2="5664" y1="2880" y2="2976" x1="5664" />
            <wire x2="5664" y1="2976" y2="2976" x1="5600" />
            <wire x2="5600" y1="2976" y2="4000" x1="5600" />
        </branch>
    </sheet>
</drawing>