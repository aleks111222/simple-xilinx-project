<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="INPUT(7:0)" />
        <signal name="OUTPUT(15:0)" />
        <signal name="OUTPUT(7)" />
        <signal name="OUTPUT(6)" />
        <signal name="OUTPUT(5)" />
        <signal name="OUTPUT(4)" />
        <signal name="OUTPUT(3)" />
        <signal name="OUTPUT(2)" />
        <signal name="OUTPUT(1)" />
        <signal name="OUTPUT(0)" />
        <signal name="INPUT(7)" />
        <signal name="INPUT(6)" />
        <signal name="INPUT(5)" />
        <signal name="INPUT(4)" />
        <signal name="INPUT(3)" />
        <signal name="INPUT(2)" />
        <signal name="INPUT(1)" />
        <signal name="INPUT(0)" />
        <signal name="OUTPUT(8)" />
        <signal name="OUTPUT(9)" />
        <signal name="OUTPUT(10)" />
        <signal name="OUTPUT(11)" />
        <signal name="OUTPUT(12)" />
        <signal name="OUTPUT(13)" />
        <signal name="OUTPUT(14)" />
        <signal name="OUTPUT(15)" />
        <port polarity="Input" name="INPUT(7:0)" />
        <port polarity="Output" name="OUTPUT(15:0)" />
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="buf" name="XLXI_1">
            <blockpin signalname="INPUT(7)" name="I" />
            <blockpin signalname="OUTPUT(7)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_2">
            <blockpin signalname="INPUT(6)" name="I" />
            <blockpin signalname="OUTPUT(6)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_3">
            <blockpin signalname="INPUT(5)" name="I" />
            <blockpin signalname="OUTPUT(5)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_4">
            <blockpin signalname="INPUT(4)" name="I" />
            <blockpin signalname="OUTPUT(4)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_5">
            <blockpin signalname="INPUT(3)" name="I" />
            <blockpin signalname="OUTPUT(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_6">
            <blockpin signalname="INPUT(2)" name="I" />
            <blockpin signalname="OUTPUT(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_7">
            <blockpin signalname="INPUT(1)" name="I" />
            <blockpin signalname="OUTPUT(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_8">
            <blockpin signalname="INPUT(0)" name="I" />
            <blockpin signalname="OUTPUT(0)" name="O" />
        </block>
        <block symbolname="gnd" name="XLXI_17">
            <blockpin signalname="OUTPUT(8)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_18">
            <blockpin signalname="OUTPUT(9)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_19">
            <blockpin signalname="OUTPUT(10)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_20">
            <blockpin signalname="OUTPUT(11)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="OUTPUT(12)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_22">
            <blockpin signalname="OUTPUT(13)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_23">
            <blockpin signalname="OUTPUT(14)" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_24">
            <blockpin signalname="OUTPUT(15)" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="INPUT(7:0)">
            <wire x2="672" y1="608" y2="608" x1="416" />
            <wire x2="672" y1="608" y2="704" x1="672" />
            <wire x2="672" y1="704" y2="848" x1="672" />
            <wire x2="672" y1="848" y2="976" x1="672" />
            <wire x2="672" y1="976" y2="1120" x1="672" />
            <wire x2="672" y1="1120" y2="1248" x1="672" />
            <wire x2="672" y1="1248" y2="1424" x1="672" />
            <wire x2="672" y1="1424" y2="1584" x1="672" />
            <wire x2="672" y1="1584" y2="1728" x1="672" />
            <wire x2="672" y1="1728" y2="2080" x1="672" />
            <wire x2="672" y1="2080" y2="2080" x1="656" />
        </branch>
        <iomarker fontsize="28" x="416" y="608" name="INPUT(7:0)" orien="R180" />
        <iomarker fontsize="28" x="2832" y="640" name="OUTPUT(15:0)" orien="R0" />
        <branch name="OUTPUT(15:0)">
            <wire x2="2304" y1="2496" y2="2496" x1="2288" />
            <wire x2="2832" y1="640" y2="640" x1="2304" />
            <wire x2="2304" y1="640" y2="800" x1="2304" />
            <wire x2="2304" y1="800" y2="912" x1="2304" />
            <wire x2="2304" y1="912" y2="1024" x1="2304" />
            <wire x2="2304" y1="1024" y2="1120" x1="2304" />
            <wire x2="2304" y1="1120" y2="1248" x1="2304" />
            <wire x2="2304" y1="1248" y2="1376" x1="2304" />
            <wire x2="2304" y1="1376" y2="1488" x1="2304" />
            <wire x2="2304" y1="1488" y2="1632" x1="2304" />
            <wire x2="2304" y1="1632" y2="1744" x1="2304" />
            <wire x2="2304" y1="1744" y2="1840" x1="2304" />
            <wire x2="2304" y1="1840" y2="1952" x1="2304" />
            <wire x2="2304" y1="1952" y2="2064" x1="2304" />
            <wire x2="2304" y1="2064" y2="2160" x1="2304" />
            <wire x2="2304" y1="2160" y2="2272" x1="2304" />
            <wire x2="2304" y1="2272" y2="2368" x1="2304" />
            <wire x2="2304" y1="2368" y2="2448" x1="2304" />
            <wire x2="2304" y1="2448" y2="2496" x1="2304" />
        </branch>
        <bustap x2="768" y1="704" y2="704" x1="672" />
        <bustap x2="768" y1="848" y2="848" x1="672" />
        <bustap x2="768" y1="976" y2="976" x1="672" />
        <bustap x2="768" y1="1120" y2="1120" x1="672" />
        <bustap x2="768" y1="1248" y2="1248" x1="672" />
        <bustap x2="768" y1="1424" y2="1424" x1="672" />
        <bustap x2="768" y1="1584" y2="1584" x1="672" />
        <bustap x2="768" y1="1728" y2="1728" x1="672" />
        <bustap x2="2208" y1="2448" y2="2448" x1="2304" />
        <bustap x2="2208" y1="2368" y2="2368" x1="2304" />
        <bustap x2="2208" y1="2272" y2="2272" x1="2304" />
        <bustap x2="2208" y1="2160" y2="2160" x1="2304" />
        <bustap x2="2208" y1="2064" y2="2064" x1="2304" />
        <bustap x2="2208" y1="1952" y2="1952" x1="2304" />
        <bustap x2="2208" y1="1840" y2="1840" x1="2304" />
        <bustap x2="2208" y1="1744" y2="1744" x1="2304" />
        <bustap x2="2208" y1="1632" y2="1632" x1="2304" />
        <bustap x2="2208" y1="1488" y2="1488" x1="2304" />
        <bustap x2="2208" y1="1376" y2="1376" x1="2304" />
        <bustap x2="2208" y1="1248" y2="1248" x1="2304" />
        <bustap x2="2208" y1="1120" y2="1120" x1="2304" />
        <bustap x2="2208" y1="1024" y2="1024" x1="2304" />
        <bustap x2="2208" y1="912" y2="912" x1="2304" />
        <bustap x2="2208" y1="800" y2="800" x1="2304" />
        <instance x="1264" y="592" name="XLXI_1" orien="R0" />
        <instance x="1280" y="752" name="XLXI_2" orien="R0" />
        <instance x="1248" y="928" name="XLXI_3" orien="R0" />
        <instance x="1232" y="1088" name="XLXI_4" orien="R0" />
        <instance x="1232" y="1264" name="XLXI_5" orien="R0" />
        <instance x="1232" y="1440" name="XLXI_6" orien="R0" />
        <instance x="1120" y="1616" name="XLXI_7" orien="R0" />
        <instance x="976" y="1760" name="XLXI_8" orien="R0" />
        <branch name="OUTPUT(7)">
            <wire x2="1888" y1="560" y2="560" x1="1488" />
            <wire x2="1888" y1="560" y2="800" x1="1888" />
            <wire x2="2208" y1="800" y2="800" x1="1888" />
        </branch>
        <branch name="OUTPUT(6)">
            <wire x2="1856" y1="720" y2="720" x1="1504" />
            <wire x2="1856" y1="720" y2="912" x1="1856" />
            <wire x2="2208" y1="912" y2="912" x1="1856" />
        </branch>
        <branch name="OUTPUT(5)">
            <wire x2="1808" y1="896" y2="896" x1="1472" />
            <wire x2="1808" y1="896" y2="1024" x1="1808" />
            <wire x2="2208" y1="1024" y2="1024" x1="1808" />
        </branch>
        <branch name="OUTPUT(4)">
            <wire x2="1456" y1="1056" y2="1088" x1="1456" />
            <wire x2="1824" y1="1088" y2="1088" x1="1456" />
            <wire x2="1824" y1="1088" y2="1120" x1="1824" />
            <wire x2="2208" y1="1120" y2="1120" x1="1824" />
        </branch>
        <branch name="OUTPUT(3)">
            <wire x2="1824" y1="1232" y2="1232" x1="1456" />
            <wire x2="1824" y1="1232" y2="1248" x1="1824" />
            <wire x2="2208" y1="1248" y2="1248" x1="1824" />
        </branch>
        <branch name="OUTPUT(2)">
            <wire x2="1824" y1="1408" y2="1408" x1="1456" />
            <wire x2="1824" y1="1376" y2="1408" x1="1824" />
            <wire x2="2208" y1="1376" y2="1376" x1="1824" />
        </branch>
        <branch name="OUTPUT(1)">
            <wire x2="1776" y1="1584" y2="1584" x1="1344" />
            <wire x2="1776" y1="1488" y2="1584" x1="1776" />
            <wire x2="2208" y1="1488" y2="1488" x1="1776" />
        </branch>
        <branch name="OUTPUT(0)">
            <wire x2="1696" y1="1728" y2="1728" x1="1200" />
            <wire x2="1696" y1="1632" y2="1728" x1="1696" />
            <wire x2="2208" y1="1632" y2="1632" x1="1696" />
        </branch>
        <branch name="INPUT(7)">
            <wire x2="1008" y1="704" y2="704" x1="768" />
            <wire x2="1008" y1="560" y2="704" x1="1008" />
            <wire x2="1264" y1="560" y2="560" x1="1008" />
        </branch>
        <branch name="INPUT(6)">
            <wire x2="1024" y1="848" y2="848" x1="768" />
            <wire x2="1024" y1="720" y2="848" x1="1024" />
            <wire x2="1280" y1="720" y2="720" x1="1024" />
        </branch>
        <branch name="INPUT(5)">
            <wire x2="1008" y1="976" y2="976" x1="768" />
            <wire x2="1008" y1="896" y2="976" x1="1008" />
            <wire x2="1248" y1="896" y2="896" x1="1008" />
        </branch>
        <branch name="INPUT(4)">
            <wire x2="992" y1="1120" y2="1120" x1="768" />
            <wire x2="992" y1="1056" y2="1120" x1="992" />
            <wire x2="1232" y1="1056" y2="1056" x1="992" />
        </branch>
        <branch name="INPUT(3)">
            <wire x2="992" y1="1248" y2="1248" x1="768" />
            <wire x2="992" y1="1232" y2="1248" x1="992" />
            <wire x2="1232" y1="1232" y2="1232" x1="992" />
        </branch>
        <branch name="INPUT(2)">
            <wire x2="992" y1="1424" y2="1424" x1="768" />
            <wire x2="992" y1="1408" y2="1424" x1="992" />
            <wire x2="1232" y1="1408" y2="1408" x1="992" />
        </branch>
        <branch name="INPUT(1)">
            <wire x2="1120" y1="1584" y2="1584" x1="768" />
        </branch>
        <branch name="INPUT(0)">
            <wire x2="976" y1="1728" y2="1728" x1="768" />
        </branch>
        <instance x="2160" y="2672" name="XLXI_17" orien="R0" />
        <instance x="1920" y="2672" name="XLXI_19" orien="R0" />
        <instance x="1776" y="2688" name="XLXI_20" orien="R0" />
        <instance x="1632" y="2672" name="XLXI_21" orien="R0" />
        <instance x="1472" y="2656" name="XLXI_22" orien="R0" />
        <instance x="1232" y="2656" name="XLXI_23" orien="R0" />
        <instance x="1056" y="2672" name="XLXI_24" orien="R0" />
        <branch name="OUTPUT(8)">
            <wire x2="2208" y1="2448" y2="2448" x1="2176" />
            <wire x2="2176" y1="2448" y2="2528" x1="2176" />
            <wire x2="2224" y1="2528" y2="2528" x1="2176" />
            <wire x2="2224" y1="2528" y2="2544" x1="2224" />
        </branch>
        <instance x="2016" y="2672" name="XLXI_18" orien="R0" />
        <branch name="OUTPUT(9)">
            <wire x2="2208" y1="2368" y2="2368" x1="2080" />
            <wire x2="2080" y1="2368" y2="2544" x1="2080" />
        </branch>
        <branch name="OUTPUT(10)">
            <wire x2="2208" y1="2272" y2="2272" x1="1984" />
            <wire x2="1984" y1="2272" y2="2544" x1="1984" />
        </branch>
        <branch name="OUTPUT(11)">
            <wire x2="2208" y1="2160" y2="2160" x1="1840" />
            <wire x2="1840" y1="2160" y2="2560" x1="1840" />
        </branch>
        <branch name="OUTPUT(12)">
            <wire x2="2208" y1="2064" y2="2064" x1="1696" />
            <wire x2="1696" y1="2064" y2="2544" x1="1696" />
        </branch>
        <branch name="OUTPUT(13)">
            <wire x2="2208" y1="1952" y2="1952" x1="1536" />
            <wire x2="1536" y1="1952" y2="2528" x1="1536" />
        </branch>
        <branch name="OUTPUT(14)">
            <wire x2="2208" y1="1840" y2="1840" x1="1296" />
            <wire x2="1296" y1="1840" y2="2528" x1="1296" />
        </branch>
        <branch name="OUTPUT(15)">
            <wire x2="1120" y1="2464" y2="2544" x1="1120" />
            <wire x2="1248" y1="2464" y2="2464" x1="1120" />
            <wire x2="1248" y1="1776" y2="2464" x1="1248" />
            <wire x2="2208" y1="1776" y2="1776" x1="1248" />
            <wire x2="2208" y1="1744" y2="1776" x1="2208" />
        </branch>
    </sheet>
</drawing>