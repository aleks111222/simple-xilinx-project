<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="aspartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="COND" />
        <signal name="OUT1" />
        <signal name="OUT0" />
        <signal name="OP" />
        <signal name="XLXN_5" />
        <port polarity="Input" name="COND" />
        <port polarity="Output" name="OUT1" />
        <port polarity="Output" name="OUT0" />
        <port polarity="Input" name="OP" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="and2b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="40" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="52" cy="-64" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="COND" name="I0" />
            <blockpin signalname="OP" name="I1" />
            <blockpin signalname="OUT1" name="O" />
        </block>
        <block symbolname="and2b1" name="XLXI_3">
            <blockpin signalname="COND" name="I0" />
            <blockpin signalname="OP" name="I1" />
            <blockpin signalname="OUT0" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1296" y="1344" name="XLXI_1" orien="R0" />
        <instance x="1296" y="1584" name="XLXI_3" orien="R0" />
        <branch name="COND">
            <wire x2="1120" y1="1520" y2="1520" x1="880" />
            <wire x2="1296" y1="1520" y2="1520" x1="1120" />
            <wire x2="1296" y1="1280" y2="1280" x1="1120" />
            <wire x2="1120" y1="1280" y2="1520" x1="1120" />
        </branch>
        <branch name="OUT1">
            <wire x2="1792" y1="1248" y2="1248" x1="1552" />
        </branch>
        <branch name="OUT0">
            <wire x2="1792" y1="1488" y2="1488" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1792" y="1248" name="OUT1" orien="R0" />
        <iomarker fontsize="28" x="1792" y="1488" name="OUT0" orien="R0" />
        <branch name="OP">
            <wire x2="1040" y1="1216" y2="1216" x1="880" />
            <wire x2="1296" y1="1216" y2="1216" x1="1040" />
            <wire x2="1040" y1="1216" y2="1440" x1="1040" />
            <wire x2="1040" y1="1440" y2="1456" x1="1040" />
            <wire x2="1296" y1="1456" y2="1456" x1="1040" />
        </branch>
        <iomarker fontsize="28" x="880" y="1520" name="COND" orien="R180" />
        <iomarker fontsize="28" x="880" y="1216" name="OP" orien="R180" />
    </sheet>
</drawing>